"""using flag arguments"""
from dataclasses import dataclass
from enum import StrEnum, auto


FIXED_FREE_DAYS = 6


class Role(StrEnum):
    PRES = auto()
    VCEPRES = auto()
    MANAGER = auto()
    LEAD = auto()
    ENGINEER = auto()
    INTERN = auto()


@dataclass
class Employee:
    name: str
    role: Role
    free_days: int = 26

    def take_free_day(self, payout: bool, no_of_days: int = 1) -> None:  # flag as a parameter of this method
        # actually it looks like it requires 2 functions - 1 for true, 1 for false
        if payout:
            if self.free_days < FIXED_FREE_DAYS:
                raise ValueError(
                    f'Not enough free days left for payout. Remaining days: {self.free_days}'
                )
            self.free_days -= FIXED_FREE_DAYS
            print(f'Free days deducted. Free days remaining: {self.free_days}')
        else:
            if self.free_days < no_of_days:
                raise ValueError('Not enough free days !')
            self.free_days -= no_of_days
            print('You are free to go !')


def main() -> None:
    employee = Employee(name='Ian', role=Role.MANAGER)
    employee.take_free_day(True)


if __name__ == '__main__':
    main()





