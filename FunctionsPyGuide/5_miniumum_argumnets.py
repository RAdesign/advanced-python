"""corrected version - with minimized amount of parameters, using Protocol, class separation"""
from dataclasses import dataclass
from datetime import datetime
from typing import Protocol


def checksum_luhn_algo(credit_card_number: str) -> bool:
    def digits_of(number: str) -> list[int]:
        return [int(dg) for dg in number]

    digits = digits_of(credit_card_number)
    odd_digits = digits[-1::-2]
    even_digits = digits[-2::-2]
    checksum = 0
    checksum += sum(odd_digits)
    for digit in even_digits:
        checksum += sum(digits_of(str(digit*2)))
    return checksum % 10 == 0


# separation of information into new classes
@dataclass
class Card:
    credit_card_number: str
    credit_card_expiry_month: int
    credit_card_expiry_year: int
    credit_card_is_valid: bool = False


@dataclass
class Customer:
    name: str
    phone: str
    credit_card: Card
    credit_card_is_valid: bool = False


class CardInfo(Protocol):  # using Protocol+properties to control parameters use
    @property
    def credit_card_number(self) -> str:
        ...

    @property
    def credit_card_expiry_month(self) -> int:
        ...

    @property
    def credit_card_expiry_year(self) -> int:
        ...


def validate_card(card: CardInfo) -> bool:  # because of Protocol abstraction, this function has no access to Customer

    return checksum_luhn_algo(card.credit_card_number) \
           and datetime(card.credit_card_expiry_year, card.credit_card_expiry_month, 1) > datetime.now()


def main() -> None:
    card = Card(credit_card_number='4912190007575069',
                credit_card_expiry_year=2024,
                credit_card_expiry_month=2,
                )
    winnie = Customer(
        name='Winnie',
        phone='999333111',
        credit_card=card
    )
    card.credit_card_is_valid = validate_card(card)  # now no need to pass parameters again
    print(f'Winnie`s card valid:{card.credit_card_is_valid} ')
    print(winnie)


if __name__ == '__main__':
    main()