"""using and creating objects in the same place"""
import logging


class SkrillPaymentHandler:
    def handle_payment(self, amount: int) -> None:
        logging.info(f'Charged amount: {amount / 100:.2f} using Skrill')


PRICES = {
    'item_one': 15_00,
    'item_two': 10_00,
    'item_three': 5_00,
    'item_four': 1_00,
}


# using and creating in one place, hard for testing
def order_items(items: list[str]) -> None:
    total_sum = sum(PRICES[item] for item in items)
    logging.info(f'Total order is {total_sum / 100:.2f}')
    payment_handler = SkrillPaymentHandler()  # creates payment handler, hard to change to other handlers
    payment_handler.handle_payment(total_sum)  # uses payment handler
    logging.info('Order completed')


def main() -> None:
    logging.basicConfig(level=logging.INFO)
    order_items(['item_one', 'item_two', 'item_four'])


if __name__ == '__main__':
    main()
