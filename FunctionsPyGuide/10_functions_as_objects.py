"""functions are objects - with partial and others"""
import logging
from functools import partial
from typing import Callable


def handle_payment_skrill(amount: int) -> None:  # function instead of class object
    logging.info(f'Charged amount: {amount / 100:.2f} using Skrill')


PRICES = {
    'item_one': 15_00,
    'item_two': 10_00,
    'item_three': 5_00,
    'item_four': 1_00,
}

HandlePaymentFn = Callable[[int], None]  # instead of Protocol - a type alias


def order_items(items: list[str], payment_handler: HandlePaymentFn) -> None:  # now a function as a parameter
    total_sum = sum(PRICES[item] for item in items)
    logging.info(f'Total order is {total_sum / 100:.2f}')
    payment_handler(total_sum)
    logging.info('Order completed')


# function as a key parameter, with partial use of function
order_items_skrill = partial(order_items, payment_handler=handle_payment_skrill)


def main() -> None:
    logging.basicConfig(level=logging.INFO)
    # order_items(['item_one', 'item_two', 'item_four'], handle_payment_skrill)  # old version without partial
    order_items_skrill(['item_one', 'item_two', 'item_four'])  # partial provides payment handler function argument


if __name__ == '__main__':
    main()
