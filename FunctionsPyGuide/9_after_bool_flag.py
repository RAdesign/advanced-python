"""using flag arguments - fixed"""
from dataclasses import dataclass
from enum import StrEnum, auto


FIXED_FREE_DAYS = 6


class Role(StrEnum):
    PRES = auto()
    VCEPRES = auto()
    MANAGER = auto()
    LEAD = auto()
    ENGINEER = auto()
    INTERN = auto()


@dataclass
class Employee:
    name: str
    role: str
    free_days: int = 26

    # now there are 2 methods , instead of 1 with a flag
    def payout_for_holidays(self) -> None:
        if self.free_days < FIXED_FREE_DAYS:
            raise ValueError(
                f'Not enough free days left for payout. Remaining days: {self.free_days}'
            )
        self.free_days -= FIXED_FREE_DAYS
        print(f'Free days deducted. Free days remaining: {self.free_days}')

    def take_free_day(self, no_of_days: int = 1) -> None:
        if self.free_days < no_of_days:
            raise ValueError('Not enough free days !')
        self.free_days -= no_of_days
        print('You are free to go !')


def main() -> None:
    employee = Employee(name='Ian', role=Role.MANAGER)
    employee.payout_for_holidays()


if __name__ == '__main__':
    main()





