"""version of functions before fixes"""
from dataclasses import dataclass
from datetime import datetime


@dataclass
class Customer:
    name: str
    phone: str
    credit_card_number: str
    credit_card_expiry_month: int
    credit_card_expiry_year: int
    credit_card_is_valid: bool = False


def validate_card(customer: Customer) -> bool:  # Customer as a parameter is too much
    # function does many things - handles clearing numbers and assignments, and then the checksum algorithm
    def digits_of(number: str) -> list[int]:
        return [int(dg) for dg in number]

    digits = digits_of(customer.credit_card_number)
    odd_digits = digits[-1::-2]
    even_digits = digits[-2::-2]
    checksum = 0
    checksum += sum(odd_digits)
    for digit in even_digits:
        checksum += sum(digits_of(str(digit*2)))

    customer.credit_card_is_valid = (
            checksum % 10 == 0 and
            datetime(customer.credit_card_expiry_year, customer.credit_card_expiry_month, 1) > datetime.now()
    )

    return customer.credit_card_is_valid


def main() -> None:
    winnie = Customer(
        name='Winnie',
        phone='999333111',
        credit_card_number='4912190007575069',
        credit_card_expiry_month=2,
        credit_card_expiry_year=2024,
    )
    is_valid = validate_card(winnie)
    print(f'Winnie`s card valid:{is_valid} ')
    print(winnie)


if __name__ == '__main__':
    main()