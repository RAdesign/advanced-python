"""version of functions after fixes, using minimal - keyword - information for validate_card """
from dataclasses import dataclass
from datetime import datetime


def checksum_luhn_algo(credit_card_number: str) -> bool:
    def digits_of(number: str) -> list[int]:
        return [int(dg) for dg in number]

    digits = digits_of(credit_card_number)
    odd_digits = digits[-1::-2]
    even_digits = digits[-2::-2]
    checksum = 0
    checksum += sum(odd_digits)
    for digit in even_digits:
        checksum += sum(digits_of(str(digit*2)))
    return checksum % 10 == 0


@dataclass
class Customer:
    name: str
    phone: str
    credit_card_number: str
    credit_card_expiry_month: int
    credit_card_expiry_year: int
    credit_card_is_valid: bool = False


# this function does not need full Customer information, changed specific variables '*' to use keyword arguments
def validate_card(*, number: str, exp_mo: int, exp_y: int) -> bool:

    return checksum_luhn_algo(number) and datetime(exp_y, exp_mo,  1) > datetime.now()


def main() -> None:
    winnie = Customer(
        name='Winnie',
        phone='999333111',
        credit_card_number='4912190007575069',
        credit_card_expiry_month=2,
        credit_card_expiry_year=2024,
    )
    # this version needs to unpack all the information and get it while customer creation
    winnie.credit_card_is_valid = validate_card(number=winnie.credit_card_number,
                                                exp_y=winnie.credit_card_expiry_year,
                                                exp_mo=winnie.credit_card_expiry_month,)
    print(f'Winnie`s card valid:{winnie.credit_card_is_valid} ')
    print(winnie)


if __name__ == '__main__':
    main()