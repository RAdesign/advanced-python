"""using and creating objects in the same place - corrected with dependency injection, Protocol class for abstraction"""
import logging
from typing import Protocol


class SkrillPaymentHandler:
    def handle_payment(self, amount: int) -> None:
        logging.info(f'Charged amount: {amount/100:.2f} using Skrill')


PRICES = {
    'item_one': 15_00,
    'item_two': 10_00,
    'item_three': 5_00,
    'item_four': 1_00,
}


class PaymentHandler(Protocol):  # Protocol handles any abstract payment handler now
    def handle_payment(self, amount: int) -> None:
        ...


def order_items(items: list[str], payment_handler: PaymentHandler) -> None:  # dependency injection
    total_sum = sum(PRICES[item] for item in items)
    logging.info(f'Total order is {total_sum/100:.2f}')
    payment_handler.handle_payment(total_sum)
    logging.info('Order completed')


def main() -> None:
    logging.basicConfig(level=logging.INFO)
    order_items(['item_one', 'item_two', 'item_four'], SkrillPaymentHandler())  # separation of use and creation


if __name__ == '__main__':
    main()
    