"""basic raw query with users input parameter '?' - but there are SQL injection attack vulnerability, unless
properly secured with good syntax"""
import sqlite3


def main() -> None:
    number_of_customers = int(input('How many customers to add to query?'))

    connection = sqlite3.connect('db/sample_database.db')

    cursor = connection.cursor()

    # basic sequel query, customers who spend the most money
    sql_raw = """
        SELECT
                c.id,
                c.first_name,
                SUM(i.total) AS total
        FROM Invoice i
        LEFT JOIN Customer c ON i.customer_id = c.id
        GROUP BY c.id, c.first_name
        ORDER BY total DESC
        LIMIT ?;
        """

    for row in cursor.execute(sql_raw, (number_of_customers,)):
        print(row)


if __name__ == '__main__':
    main()
