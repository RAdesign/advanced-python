"""sequel query builder is a kind of interface built on top of a query, to make it easier to process, and much safer,
using pypika library, (for typescript - Knex.js)."""
import sqlite3

from pypika import Order, Query, Table, functions


def main() -> None:
    number_of_customers = int(input('How many customers to add to query?'))

    # no need to specify a complicated schema, as in ORM, but no help from IDE
    invoice = Table('Invoice')
    customer = Table('Customer')
    query = (
        Query.from_(invoice)
        .left_join(customer)
        .on(invoice.customer_id == customer.id)
        .groupby(customer.id, customer.first_name)
        .orderby(functions.Sum(invoice.total), order=Order.desc)
        .limit(number_of_customers)
        .select(
            customer.id,
            customer.first_name,
            functions.Sum(invoice.total).as_('total'),
        )
    )

    connection = sqlite3.connect('db/sample_database.db')

    cursor = connection.cursor()

    sql = query.get_sql()

    for row in cursor.execute(sql):
        print(row)


if __name__ == '__main__':
    main()
