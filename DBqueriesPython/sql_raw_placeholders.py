"""basic SQL query, but with a placeholder, to safeguard against SQL injection attacks"""
import sqlite3


def main() -> None:
    number_of_customers = int(input('How many customers to add to query?'))

    connection = sqlite3.connect('db/sample_database.db')

    cursor = connection.cursor()

    sql_raw = """
        SELECT
                c.id,
                c.first_name,
                SUM(i.total) AS total
        FROM Invoice i
        LEFT JOIN Customer c ON i.customer_id = c.id
        GROUP BY c.id, c.first_name
        ORDER BY total DESC
        LIMIT :limit;
        """

    placeholder = {'limit': number_of_customers}

    for row in cursor.execute(sql_raw, placeholder):
        print(row)


if __name__ == '__main__':
    main()
