"""basic SQL query, but with a file placeholder, to safeguard against SQL injection attacks,
external file contains a query"""
import sqlite3
from pathlib import Path


def read_sql_query(sql_path: str) -> str:
    # read SQL file as a string
    return Path(sql_path).read_text()


def main() -> None:
    number_of_customers = int(input('How many customers to add to query?'))

    connection = sqlite3.connect('db/sample_database.db')

    cursor = connection.cursor()

    sql_raw = read_sql_query('sql/customers.sql')

    placeholder = {'limit': number_of_customers}

    for row in cursor.execute(sql_raw, placeholder):
        print(row)


if __name__ == '__main__':
    main()
