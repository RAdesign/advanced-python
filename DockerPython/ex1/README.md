1.difference
-dockerfile: blueprint for building images
-image: template for running containers
-container: running process with the packaged project

2.build docker image

$ docker build -t python-imdb

3.run the docker image (starts the container)
without user input:

$ docker run python-imdb

with user input(comment out the 'break' in main.py)

$ docker run -t -i python imdb

(-i: interactive, -t: pseudo terminal)
