import random
import requests
from bs4 import BeautifulSoup

# crawl IMDB for top 250 movies and randomly select one

URL = 'http://www.imbd.com/chart/top'


def main():
    response = requests.get(URL)

    soup = BeautifulSoup(response.text, 'html.parser')

    # print(soup.prettify())

    movietags = soup.select('td.titleColumn')
    inner_movietags = soup.select('td.titleColumn a')
    ratingtags = soup.select('td.posterColumn span[name=ir')

    def get_year(movie_tag):
        moviesplit = movie_tag.text.split()
        year = moviesplit[-1]  # last item
        return year

    years = [get_year(tag) for tag in movietags]
    actors_list = [tag['title'] for tag in inner_movietags]  # access attr 'title'
    titles = [tag.text for tag in inner_movietags]
    ratings = [float(tag['data-value']) for tag in ratingtags]  # access attr 'data-value'

    n_movies = len(titles)

    while True:
        indx = random.randrange(0, n_movies)

        print(f'{titles[indx]} {years[indx]}, Rating: {ratings[indx]:.1f}, Starring: {actors_list[indx]}')

        # comment the next line out to test user input with docker run -t -i
        break

        user_input = input("Do you want to search for another movie? (y/[n]).")
        if user_input != 'y':
            break


if __name__ == "__main__":
    main()
