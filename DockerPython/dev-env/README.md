1.Dockerize an app:
FROM python:3.11-slim
WORKDIR /code
COPY ./requirements.txt ./
RUN pip install --no-cache-dir --upgrade -r requirements.txt
COPY ./src ./src
CMD ["uvicorn", "src.main:app", "--host", "0.0.0.0", "--port", "80", "--reload"]

use dockerfile and code in src directory
use slim or alpine for smaller versions

docker build -t fastapi -image .   #builds docker image
docker images   #checks for docker images present
docker run --name fastapi-container -p 80:80 fastapi-image
docker rm fastapi-container  #cleanup for another setup
docker run -d --name fastapi-container -p 80:80 fastapi-image  #detached mode

2.Immediate file changes (volumes) - do the mapping of code on machine and container

docker stop fastapi-container #stop running
docker ps -a  #checks if container is still running
docker rm fastapi-container  #cleanup
docker run -d --name fastapi-container -p 80:80 -v $(pwd):/code fastapi-image  #-v for volume, access, local directory, where code is

3.use IDE in docker
4.docker compose

services:
    app:
        build: .
        container_name: python-server
        command: uvicorn src.main:app --host 0.0.0.0 --port 80 --reload
        ports:
         - 80:80
         - 5678:5678
        volumes:
         - .:/code

Use docker-compose.yml

docker-compose up  #one command to run the entire setup from yaml file
docker-compose down  #remove the docker env

5.add more services(Redis)

services:
    app:
     ...
     depends_on:
        - redis
    redis:
     image: redis:alpine

6.debug python code inside a container

import debugpy
debugpy.listen(("0.0.0.0", 5678))
# print("Waiting for client to attach...")
# debugpy.wait_for_client()

And the port in yml:

services:
    app:
    ...
    ports:
     - 80:80
     - 5678:5678

Attach to running container

Commands for cleaning up:
docker rm container_name
docker image rm image_name
docker system prune
docker images prune