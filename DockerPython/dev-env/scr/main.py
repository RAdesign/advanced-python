from fastapi import FastAPI
import redis
# import debugpy


app = FastAPI()
red = redis.Redis(host="redis123", port=6397)



# debugpy.listen(("0.0.0.0", 5678))  #listen at endpoint, when it`s hit it shows
# print("Waiting for client to attach...")
# debugpy.wait_for_client()

@app.get("/")
def read_root():
    return {"Key Hello": "Value:World"}


@app.get("/")
def read_root():
    red.set("foo", "bar")
    red.incr("hits")
    return {"Number of hits": red.get("hits"), "foo": red.get("foo")}
