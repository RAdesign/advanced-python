# Dockerize python web app
# 1.create app locally

$ python3 -m venv venv
$ . venv/bin/activate
$ pip install fastapi uvicorn

# Try it like this:

$ uvicorn app.main:app --port 80

# Save Dependencies:

$ pip freeze > requirements.txt

# 2.build the docker image

$ docker build -t fastapi-image .
# (use .dockerignore file to ignore specific files)

# 3.run the docker image
# Normal:

$ docker run -p 80:80 fastapi-image

# Run in background , give a name:

$ docker run -d --name myfastapicontainer -p 80:80 fastapi-image

# map the port from outside to the port from the container
-p 80:80  

# host in dockerfile like this:
host: 0.0.0.0:"placeholder"
# it tells the server to listen for an accept connections from any IP address("all IPv4 addresses on the
# local machine").

