from typing import Optional

from fastapi import FastAPI

app = FastAPI()


@app.get("/")
def read_root():
    return {"key hello": "value world"}


@app.get("/items/{idem_id")
def read_item(item_id: int, q: Optional[str] = None):
    return {"item_id": item_id, "q": q}
