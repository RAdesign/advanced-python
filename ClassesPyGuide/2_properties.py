""" using properties and dunder methods to simplify using of classes"""

from dataclasses import dataclass
from functools import lru_cache
from email_tool.service import EmailService

SMTP_SERVER = "smtp.gmail.com"
PORT = 465
EMAIL = "radesign@op.pl"
PASSWORD = "mypassword"


@lru_cache  # use cached property to prevent recalculating BMI each time l(east)r(ecently)u(sed) cache
# @property  # for easier use change to property
def bmi(weight: float, height: float) -> float:
    return weight / (height ** 2)


# @property  # for easier use change to property, need to change bmi to a function for this
@lru_cache
def bmi_category(bmi_value: float) -> str:
    if bmi_value < 18.5:  # use self.bmi_value with property will cause to recalculate it each time, so cache
        return "Weight too low"
    elif bmi_value < 25:
        return "Weight normal"
    elif bmi_value < 30:
        return "Overweight"
    else:
        return "Obese"


@dataclass
class Stats:
    age: int
    gender: str
    height: float
    weight: float
    blood_type: str
    eye_colour: str
    hair_colour: str


@dataclass
class Address:
    address_line_1: str
    address_line_2: str
    city: str
    country: str
    postal_code: str

    def __str__(self) -> str:  # dunder method for representation
        return f"{self.address_line_1}, {self.address_line_2}, {self.city}, {self.country}, {self.postal_code}"


@dataclass
class Person:
    name: str
    address: Address
    email: str
    phone_number: str
    stats: Stats

    def split_name(self) -> tuple[str, str]:
        first_name, last_name = self.name.split(" ")
        return first_name, last_name

    def update_email(self, email: str) -> None:
        self.email = email
        #  send email to the new address
        email_service = EmailService(
            smtp_server=SMTP_SERVER,
            port=PORT,
            email=EMAIL,
            password=PASSWORD,
        )
        email_service.send_message(
            to_email=self.email,
            subject="Your email has been updated",
            body="Your email was updated. Take care",
        )


def main() -> None:
    #  creates a person
    address = Address(
        address_line_1="Buleczkowa Street 12/13",
        address_line_2="os.Janowiec 32",
        city="Ulinia",
        country="Polska",
        postal_code="85-474",
    )
    stats = Stats(
        age=30,
        gender="male",
        height=1.85,
        weight=83.0,
        blood_type="B+",
        eye_colour="grey",
        hair_colour="black",
    )
    person = Person(
        name="Jan Kovalsky",
        email="john.kovalsky@gmail.com",
        phone_number="987-654-321",
        address=address,
        stats=stats,
    )

    #  calculate the BMI
    bmi_value = bmi(stats.weight, stats.height)
    print(f"Your BMI is {bmi_value:.2f}")
    print(f"Your BMI category is {bmi_category(bmi_value)}")

    #  update the email
    person.update_email("jan.kovalsky@gmail.com")


if __name__ == "__main__":
    main()
