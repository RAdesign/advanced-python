"""another approach - whether the class is really needed, or not, try using module with functions this time"""
from dataclasses import dataclass
from functools import lru_cache, partial
from typing import Protocol, Callable
from email_tool.service_v2 import send_email

SMTP_SERVER = "smtp.gmail.com"
PORT = 465
EMAIL = "radesign@op.pl"
PASSWORD = "mypassword"

# disadvantage of Callable like this - can't define names of these three str parameters
# EmailSender = Callable[[str, str, str], None]  # one way to use function as parameter - a Callable


# another way to solve this is to use a class with dunder __call__method
class EmailSender(Protocol):
    def __call__(self, to_email: str, subject: str, body: str) -> None:
        ...


@lru_cache
def bmi(weight: float, height: float) -> float:
    return weight / (height ** 2)


def bmi_category(bmi_value: float) -> str:
    if bmi_value < 18.5:
        return "Weight too low"
    elif bmi_value < 25:
        return "Weight normal"
    elif bmi_value < 30:
        return "Overweight"
    else:
        return "Obese"


@dataclass
class Stats:
    age: int
    gender: str
    height: float
    weight: float
    blood_type: str
    eye_colour: str
    hair_colour: str


@dataclass
class Address:
    address_line_1: str
    address_line_2: str
    city: str
    country: str
    postal_code: str

    def __str__(self) -> str:
        return f"{self.address_line_1}, {self.address_line_2}, {self.city}, {self.country}, {self.postal_code}"


@dataclass
class Person:
    name: str
    address: Address
    email: str
    phone_number: str
    stats: Stats

    def split_name(self) -> tuple[str, str]:
        first_name, last_name = self.name.split(" ")
        return first_name, last_name

    def update_email(self, email: str, send_message: EmailSender) -> None:  # send_message_fn param for Callable
        self.email = email
        #  send email to the new address
        # send_message_fn  # for a callable
        send_message(
            to_email=email,
            subject="Your email has been updated",
            body="Your email was updated. Take care",
        )


def main() -> None:
    #  creates a person
    address = Address(
        address_line_1="Buleczkowa Street 12/13",
        address_line_2="os.Janowiec 32",
        city="Ulinia",
        country="Polska",
        postal_code="85-474",
    )
    stats = Stats(
        age=30,
        gender="male",
        height=1.85,
        weight=83.0,
        blood_type="B+",
        eye_colour="grey",
        hair_colour="black",
    )
    person = Person(
        name="Jan Kovalsky",
        email="john.kovalsky@gmail.com",
        phone_number="987-654-321",
        address=address,
        stats=stats,
    )
    print(address)

    #  calculate the BMI
    bmi_value = bmi(stats.weight, stats.height)
    print(f"Your BMI is {bmi_value:.2f}")
    print(f"Your BMI category is {bmi_category(bmi_value)}")

    # update the email address with partial
    send_message = partial(send_email, smtp_server=SMTP_SERVER, port=PORT, email=EMAIL, password=PASSWORD)

    #  update the email
    person.update_email("jan.kovalsky@gmail.com", send_message)


if __name__ == "__main__":
    main()
