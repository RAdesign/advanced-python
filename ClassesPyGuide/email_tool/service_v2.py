from smtplib import SMTP_SSL
from email.message import EmailMessage

""" new version of classless package , just with functions"""


def create_email_message(to_email: str, subject: str, body: str) -> EmailMessage:
    msg = EmailMessage()
    msg.set_content(body)
    msg["Subject"] = subject
    msg["To"] = to_email
    return msg


def send_email(
        smtp_server: str,
        port: int,
        email: str,
        password: str,
        to_email: str,
        subject: str,
        body: str,
) -> None:
    msg = create_email_message(to_email, subject, body)
    with SMTP_SSL(smtp_server, port) as server:
        # server.login(self.email, self.password)  # uncomment these lines to send message
        # server.send_message(msg, self.email)
        print("Email sent with success!")
