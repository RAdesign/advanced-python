""" encapsulation examples """


class Person:
    def __init__(self, name: str, age: int, ssn: str):
        self.name = name
        self.age = age
        self.__ssn = ssn  # private attribute

    # a public method
    def display_info(self) -> None:
        print(f"Name: {self.name}")
        print(f"Age: {self.age}")
        print(f"SSN: {self.ssn}")

    @property  # this property actually masks the social security number ssn
    def ssn(self) -> str:
        masked_ssn = "XXX-XX-" + self.__ssn[-4:]
        return masked_ssn


def main() -> None:
    #  create an instance of Person
    person1 = Person("Jan Kovalsky", 33, "85-474")

    # accessing public method
    person1.display_info()

    # accessing private attribute or method directly will raise an AttributeError
    # print(person1.__ssn)  # this will raise an AttributeError
    # print(person1._Person__ssn)  # this will work, so privacy is not that high


if __name__ == "__main__":
    main()
