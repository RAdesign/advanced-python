"""first instance of correcting class - splitting data class into several, also an Email method is being split into
separate package"""
from dataclasses import dataclass
from functools import cached_property
from email_tool.service import EmailService  # new split class in a package

SMTP_SERVER = "smtp.gmail.com"
PORT = 465
EMAIL = "radesign@op.pl"
PASSWORD = "mypassword"


@dataclass
class Stats:  # split class no 1
    age: int
    gender: str
    height: float
    weight: float
    blood_type: str
    eye_colour: str
    hair_colour: str

    @cached_property
    def bmi(self) -> float:
        return self.weight / (self.height**2)

    def get_bmi_category(self) -> str:
        if self.bmi < 18.5:
            return "Weight too low"
        elif self.bmi < 25:
            return "Weight normal"
        elif self.bmi < 30:
            return "Overweight"
        else:
            return "Obese"


@dataclass  # split class no 2
class Address:
    address_line_1: str
    address_line_2: str
    city: str
    country: str
    postal_code: str

    def get_full_address(self) -> str:
        return f"{self.address_line_1}, {self.address_line_2}, {self.city}, {self.country}, {self.postal_code}"


@dataclass  # split class no 3
class Person:
    name: str
    address: Address
    email: str
    phone_number: str
    stats: Stats

    def split_name(self) -> tuple[str, str]:
        first_name, last_name = self.name.split(" ")
        return first_name, last_name

    def update_email(self, email: str) -> None:
        self.email = email
        #  send email to the new address
        email_service = EmailService(  # using new class from a package
            smtp_server=SMTP_SERVER,
            port=PORT,
            email=EMAIL,
            password=PASSWORD,
        )
        email_service.send_message(
            to_email=self.email,
            subject="Your email has been updated",
            body="Your email was updated. Take care",
        )


def main() -> None:
    #  creates a person
    address = Address(
        address_line_1="Buleczkowa Street 12/13",
        address_line_2="os.Janowiec 32",
        city="Ulinia",
        country="Polska",
        postal_code="85-474",
    )
    stats = Stats(
        age=30,
        gender="male",
        height=1.85,
        weight=83,
        blood_type="B+",
        eye_colour="grey",
        hair_colour="black",
    )
    person = Person(
        name="Jan Kovalsky",
        email="john.kovalsky@gmail.com",
        phone_number="987-654-321",
        address=address,
        stats=stats,
    )

    #  calculate the BMI
    bmi = stats.bmi
    print(f"Your BMI is {bmi:.2f}")

    #  update the email
    person.update_email("jan.kovalsky@gmail.com")


if __name__ == "__main__":
    main()
