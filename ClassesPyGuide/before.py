"""basic example has a lot of variables, and a lot of methods - time to correct this, split things -
think of classes as either data focused or behaviour focused(grouping methods that belong together"""

from dataclasses import dataclass
from email.message import EmailMessage
from smtplib import SMTP_SSL

SMTP_SERVER = "smtp.gmail.com"
PORT = 465
EMAIL = "radesign@op.pl"
PASSWORD = "mypassword"


@dataclass
class Person:
    name: str
    age: int
    address_line_1: str
    address_line_2: str
    city: str
    country: str
    postal_code: str
    email: str
    phone_number: str
    gender: str
    height: float
    weight: float
    blood_type: str
    eye_colour: str
    hair_colour: str

    def split_name(self) -> tuple[str, str]:
        first_name, last_name = self.name.split(" ")
        return first_name, last_name

    def get_full_address(self) -> str:
        return f"{self.address_line_1}, {self.address_line_2}, {self.city}, {self.country}, {self.postal_code}"

    def get_bmi(self) -> float:
        return self.weight / (self.height**2)

    def get_bmi_category(self) -> str:
        if self.get_bmi() < 18.5:
            return "Weight too low"
        elif self.get_bmi() < 25:
            return "Weight normal"
        elif self.get_bmi() < 30:
            return "Overweight"
        else:
            return "Obese"

    def update_email(self, email: str) -> None:
        self.email = email
        #  send email to the new address
        msg = EmailMessage()
        msg.set_content("Your email was updated. Take care")
        msg["Subject"] = "Your email has been updated"
        msg["To"] = self.email

        with SMTP_SSL(SMTP_SERVER, PORT) as server:
            # server.login(EMAIL, PASSWORD)  # uncomment this to 'send' a real email
            # server.send_message(msg, EMAIL)
            pass
        print("Email sent with success")


def main() -> None:
    #  creates a person
    person = Person(
        name="Jan Kovalsky",
        age=30,
        address_line_1="Buleczkowa Street 12/13",
        address_line_2="os.Janowiec 32",
        city="Ulinia",
        country="Polska",
        postal_code="85-474",
        email="john.kovalsky@gmail.com",
        phone_number="987-654-321",
        gender="male",
        height=1.85,
        weight=83,
        blood_type="B+",
        eye_colour="grey",
        hair_colour="black",
    )

    #  calculate the BMI
    bmi = person.get_bmi()
    print(f"Your BMI is {bmi:.2f}")
    print(f"Your BMI points that you are {person.get_bmi_category()}")

    #  update the email
    person.update_email("jan.kovalsky@gmail.com")


if __name__ == "__main__":
    main()
