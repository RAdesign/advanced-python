import tkinter as tk
from tkinter import ttk
from tkinter import messagebox


class ExtraWin(tk.Toplevel):
    def __init__(self):
        super().__init__()
        self.title('Extra Window')
        self.geometry('400x400')
        ttk.Label(self, text="Extra Label").pack()
        ttk.Button(self, text="Extra Button").pack()
        ttk.Label(self, text="Second Extra Label").pack(expand=True)


def ask_yes_no():
    answer = messagebox.askquestion('Title', 'Body')
    print(answer)
    messagebox.showerror('Info Title', 'Some Info')


def create_window():
    global extra_window
    extra_window = ExtraWin()
    extra_window = tk.Toplevel()
    extra_window.title('extra window')
    extra_window.geometry('400x400')
    ttk.Label(extra_window, text='A Label').pack()
    ttk.Button(extra_window, text="A Button").pack()
    ttk.Label(extra_window, text="A Second Label").pack(expand=True)


def close_window():
    extra_window.destroy()


window = tk.Tk()
window.geometry('600x600')
window.title('Many Windows')

button_1 = ttk.Button(window, text='open main window', command=create_window)
button_1.pack(expand=True)

button_2 = ttk.Button(window, text='close main window', command=close_window)
button_2.pack(expand=True)

button_3 = ttk.Button(window, text='create y/n window', command=ask_yes_no)
button_3.pack(expand=True)

window.mainloop()
