# this is going to be a series about layouts in TkInter

import tkinter as tk
from tkinter import ttk

# window creation
window = tk.Tk()
window.title('Layout basics introduction')
window.geometry('800x640')

# widgets creation
label1 = ttk.Label(window, text='Label 1', background='blue')
label2 = ttk.Label(window, text='Label 2', background='green')

# version 1 of layout scheming
# packing
# label1.pack(side='left', expand=True, fill='y')
# label2.pack(side='right', expand=True, fill='both')

# version 2 of layout scheming
# grid setup
window.columnconfigure(0, weight=1)
window.columnconfigure(1, weight=1)
window.columnconfigure(2, weight=2)
window.rowconfigure(0, weight=1)
window.rowconfigure(1, weight=1)

label1.grid(row=0, column=1, sticky='nsew')
label2.grid(row=1, column=1, columnspan=2, sticky='nsew')

# version 3 of layout scheming
# placement
# label1.place(x=100, y=200, width=200, height=100)
# label2.place(relx=0.5, rely=0.5, relwidth=1, anchor='se')

window.mainloop()
