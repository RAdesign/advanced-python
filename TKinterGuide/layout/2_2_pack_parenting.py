# just about pack command and inheritance

import tkinter as tk
from tkinter import ttk

# window setup
window = tk.Tk()
window.title('Pack parent inheritance')
window.geometry('640x800')

# create a top frame
top_frame = ttk.Frame(window)
label1 = ttk.Label(top_frame, text='Label no.1', background='blue')
label2 = ttk.Label(top_frame, text='Label no.2', background='red')

# create middle widget
label3 = ttk.Label(top_frame, text='Label Middle no.3', background='yellow')

# create a bottom frame
bottom_frame = ttk.Frame(window)
label4 = ttk.Label(bottom_frame, text='Label Bottom no.4', background='green')
button1 = ttk.Button(bottom_frame, text='Button no.1')
button2 = ttk.Button(bottom_frame, text='Button no.2')

# create a top layout
label1.pack(side='left', fill='both', expand=True)
label2.pack(side='left', fill='x', expand=True)
top_frame.pack(fill='both', expand=True)

# create middle layout
label3.pack(expand=True)

# create bottom layout
button1.pack(side='left', expand=True, fill='both')
label4.pack(side='left', expand=True, fill='y')
button2.pack(side='left', expand=False, fill='x')
bottom_frame.pack(expand=True, fill='both', padx=22, pady=22)

# additional buttons, and frame - on the right inside of bottom_frame, buttons stacked vertically inside
# widgets
practice_frame = ttk.Frame(bottom_frame)
button3 = ttk.Button(practice_frame, text='Button no.3')
button4 = ttk.Button(practice_frame, text='Button no.4')
button5 = ttk.Button(practice_frame, text='Button no.5')

# packing
button3.pack(expand=True, fill='both')
button4.pack(expand=True, fill='both')
button5.pack(expand=True, fill='both')
practice_frame.pack(side='left', expand=True, fill='both')

window.mainloop()
