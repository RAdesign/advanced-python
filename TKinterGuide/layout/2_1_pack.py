# just about pack command

import tkinter as tk
from tkinter import ttk

# window setup
window = tk.Tk()
window.title('Pack how to')
window.geometry('640x800')

# adding widgets
label1 = ttk.Label(window, text='Label no.1', background='blue')
label2 = ttk.Label(window, text='Label no.2', background='green')
label3 = ttk.Label(window, text='Label no.3', background='red')
button = ttk.Button(window, text='Button no.1')

# setting layout, check the params 
label1.pack(side='top', expand=True, fill='both', padx=12, pady=12)
label2.pack(side='left', expand=False, fill='both')
label3.pack(side='top', expand=True, fill='y')
button.pack(side='top', expand=True, fill='both')

window.mainloop()
