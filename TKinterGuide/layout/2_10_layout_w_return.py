# segmenting layout
import tkinter as tk
from tkinter import ttk


def create_segment(parent, label_text, button_text):
    frame = ttk.Frame(master=parent)
    # grid layout
    frame.rowconfigure(0, weight=1)
    frame.columnconfigure((0, 1, 2), weight=1, uniform='a')
    # widgets
    ttk.Label(frame, text=label_text).grid(row=0, column=0, sticky='nsew')
    ttk.Button(frame, text=button_text).grid(row=0, column=1, sticky='nsew')

    return frame


class Segment(ttk.Frame):
    def __init__(self, parent, label_text, button_text, practice_text):
        super().__init__(master=parent)
        # grid layout
        self.rowconfigure(0, weight=1)
        self.columnconfigure((0, 1, 2), weight=1, uniform='a')

        # widgets
        ttk.Label(self, text=label_text).grid(row=0, column=0, sticky='nsew')
        ttk.Button(self, text=button_text).grid(row=0, column=1, sticky='nsew')
        self.create_practice_box(practice_text).grid(row=0, column=2, sticky='nsew')

        self.pack(expand=True, fill='both', padx=10, pady=10)

    def create_practice_box(self, text):
        frame = ttk.Frame(master=self)
        ttk.Entry(frame).pack(expand=True, fill='both')
        ttk.Button(frame, text=text).pack(expand=True, fill='both')
        return frame


# window
window = tk.Tk()
window.title('Returning widgets')
window.geometry('800x640')

# widgets creation Ver. 1 - using function/method, on 3rd column
# create_segment(window, 'Label', 'Button').pack(expand=True, fill='both', padx=10, pady=10)
# create_segment(window, 'Test', 'Click').pack(expand=True, fill='both', padx=10, pady=10)
# create_segment(window, 'Hello', 'Test').pack(expand=True, fill='both', padx=10, pady=10)
# create_segment(window, 'Bye', 'Launch').pack(expand=True, fill='both', padx=10, pady=10)
# create_segment(window, 'Last', 'Exit').pack(expand=True, fill='both', padx=10, pady=10)

# widgets creation Ver. 2
Segment(window, 'label', 'button', 'test')
Segment(window, 'test', 'click', 'something')
Segment(window, 'hello', 'test', 'else')
Segment(window, 'bye', 'launch', 'or')
Segment(window, 'last', 'exit', 'not')

window.mainloop()
