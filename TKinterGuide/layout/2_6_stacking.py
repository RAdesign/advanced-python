# about stacking
import tkinter as tk
from tkinter import ttk

# window setup
window = tk.Tk()
window.title('Order of Stacking')
window.geometry('640x640')

# creating widgets
label1 = ttk.Label(window, text='Label no.1', background='blue')
label2 = ttk.Label(window, text='Label no.2', background='red')
label3 = ttk.Label(window, text='Label no.3', background='green')

# manipulate of stacking order 1
# label1.lift()
# label2.lower()

# buttons with different methods of raising them
button1 = ttk.Button(window, text='Raise Label 1', command=lambda: label1.lift(aboveThis=label2))
button2 = ttk.Button(window, text='Raise Label 2', command=lambda: label2.tkraise())
button3 = ttk.Button(window, text='Raise Label 3', command=lambda: label3.tkraise())

# layout setup
label1.place(x=50, y=100, width=200, height=150)
label2.place(x=150, y=60, width=140, height=100)
label3.place(x=20, y=80, width=180, height=100)

button1.place(rely=1, relx=0.8, anchor='se')
button2.place(rely=1, relx=0.8, anchor='se')
button3.place(rely=1, relx=0.8, anchor='se')

window.mainloop()
