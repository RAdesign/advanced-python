# about grids
import tkinter as tk
from tkinter import ttk

# window setup
window = tk.Tk()
window.title('Grids')
window.geometry('800x640')

# creating widgets
label1 = ttk.Label(window, text='Label no.1', background='blue')
label2 = ttk.Label(window, text='Label no.2', background='red')
label3 = ttk.Label(window, text='Label no.3', background='green')
label4 = ttk.Label(window, text='Label no.4', background='yellow')

# defining a grid
window.columnconfigure((0, 1, 2), weight=1, uniform='a')
window.columnconfigure(3, weight=2, uniform='a')
window.rowconfigure(0, weight=1, uniform='a')
window.rowconfigure(1, weight=1, uniform='a')
window.rowconfigure(2, weight=1, uniform='a')
window.rowconfigure(3, weight=3, uniform='a')

# placing a widget
label1.grid(row=0, column=0, sticky='nsew')
label2.grid(row=1, column=1, rowspan=3, sticky='nsew')
label3.grid(row=1, column=0, columnspan=3, sticky='nsew', padx=22, pady=22)
label4.grid(row=3, column=3, sticky='se')

# adding extra buttons and entry field
button1 = ttk.Button(window, text='Button no.1')
button2 = ttk.Button(window, text='Button no.2')
entry1 = ttk.Entry(window)
# griding
button1.grid(row=0, column=3, sticky='nesw')
button2.grid(row=2, column=2, sticky='nesw')
entry1.grid(row=2, column=3, rowspan=2, sticky='nw')

window.mainloop()
