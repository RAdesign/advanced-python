# combined layouts
import tkinter as tk
# from tkinter import ttk
import ttkbootstrap as ttk

# window setup
window = ttk.Window(themename='lumen')
window.title('Combined Layouts')
window.geometry('800x640')
window.minsize(640, 400)

# main layout widgets and placement
menu_frame = ttk.Frame(window)
main_frame = ttk.Frame(window)
menu_frame.place(x=0, y=0, relwidth=0.3, relheight=1)
main_frame.place(relx=0.3, y=0, relwidth=0.7, relheight=1)

# widgets in menu
menu_button_1 = ttk.Button(menu_frame, text="Button no.1")
menu_button_2 = ttk.Button(menu_frame, text="Button no.2")
menu_button_3 = ttk.Button(menu_frame, text="Button no.3")

menu_slider_1 = ttk.Scale(menu_frame, orient='vertical')
menu_slider_2 = ttk.Scale(menu_frame, orient='vertical')

toggle_frame = ttk.Frame(menu_frame)
menu_toggle_1 = ttk.Checkbutton(toggle_frame, text='Check no.1')
menu_toggle_2 = ttk.Checkbutton(toggle_frame, text='Check no.2')

entry_1 = ttk.Entry(menu_frame)

# menu layout in a grid
menu_frame.columnconfigure((0, 1, 2), weight=1, uniform='a')
menu_frame.rowconfigure((0, 1, 2, 3, 4), weight=1, uniform='a')

menu_button_1.grid(row=0, column=0, sticky='nswe', columnspan=2, padx=4, pady=4)
menu_button_2.grid(row=0, column=2, sticky='nswe', padx=4, pady=4)
menu_button_3.grid(row=1, column=0, sticky='nsew', columnspan=3, padx=4, pady=4)

menu_button_1.grid(row=2, column=0, sticky='nsew', rowspan=2, pady=20)
menu_button_2.grid(row=2, column=2, sticky='nsew', rowspan=2, pady=20)

# toggle the layout
toggle_frame.grid(row=4, column=0, columnspan=3, sticky='nsew')
menu_toggle_1.pack(side='left', expand=True)
menu_toggle_2.pack(side='left', expand=True)

# entry layout
entry_1.place(relx=0.5, rely=0.95, relwidth=0.9, anchor='center')

# main widgets creation
entry_frame_1 = ttk.Frame(main_frame)
main_label_1 = ttk.Label(entry_frame_1, text='Label no.1', background='red')
main_button_1 = ttk.Button(entry_frame_1, text='Button no.1')

entry_frame_2 = ttk.Frame(main_frame)
main_label_2 = ttk.Label(entry_frame_2, text='Label no.2', background='green')
main_button_2 = ttk.Button(entry_frame_2, text='Button no.2')

# main layout creation
entry_frame_1.pack(side='left', expand=True, fill='both', padx=20, pady=20)
entry_frame_2.pack(side='left', expand=True, fill='both', padx=20, pady=20)

main_label_1.pack(expand=True, fill='both')
main_button_1.pack(expand=True, fill='both', pady=10)

main_label_2.pack(expand=True, fill='both')
main_button_2.pack(expand=True, fill='both', pady=10)

window.mainloop()
