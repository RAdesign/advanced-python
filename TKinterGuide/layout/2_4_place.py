# about placing
import tkinter as tk
from tkinter import ttk

# window setup
window = tk.Tk()
window.title('Placing')
window.geometry('640x800')

# creating widgets
label1 = ttk.Label(window, text='Label no.1', background='blue')
label2 = ttk.Label(window, text='Label no.2', background='red')
label3 = ttk.Label(window, text='Label no.3', background='green')
button1 = ttk.Button(window, text='Button no.1')

# creating layout
label1.place(x=300, y=100, width=100, height=200)
label2.place(relx=0.2, rely=0.1, relwidth=0.4, relheight=0.5)
label3.place(x=80, y=60, width=160, height=300)
button1.place(relx=1, rely=1, anchor='se')

# creating a frame
frame1 = ttk.Frame(window)
frame1_label = ttk.Label(frame1, text='Frame_1 Label', background='yellow')
frame1_button = ttk.Button(frame1_label, text='Frame_1 Button')

# frame_1 layout
frame1.place(relx=0, rely=0, relwidth=0.3, relheight=1)
frame1_label.place(relx=0, rely=0, relwidth=1, relheight=0.5)
frame1_button.place(relx=0, rely=0.5, relwidth=1, relheight=0.5)

# additional label in the center of a window, of half windows width and 200px tall
center_label = ttk.Label(window, text='Central Label', background='grey')
center_label.place(x=320, rely=0.5, anchor='center', relwidth=0.5, height=200)

window.mainloop()
