import tkinter as tk
from tkinter import ttk
from random import randint, choice

# window setup
window = tk.Tk()
window.geometry('800x640')
window.title('Scrolling')

# canvas with random rectangles
canvas = tk.Canvas(window, bg='white', scrollregion=(0, 0, 2000, 5000))
canvas.create_line(0, 0, 2000, 5000, fill='green', width=10)
for i in range(100):
    le = randint(0, 2000)
    te = randint(0, 5000)
    re = le + randint(10, 500)
    be = te + randint(10, 500)
    colour = choice(('red', 'green', 'blue', 'yellow', 'pink', 'orange'))
    canvas.create_rectangle(le, te, re, be, fill=colour)
canvas.pack(expand=True, fill='both')

# mousewheel scrolling
canvas.bind('<MouseWheel>', lambda event: canvas.yview_scroll(-int(event.delta/60), "units"))

# vertical scrollbar
scrollbar = ttk.Scrollbar(window, orient='vertical', command=canvas.yview)
canvas.configure(yscrollcommand=scrollbar.set)
scrollbar.place(relx=1, rely=0, relheight=1, anchor='ne')

# horizontal scrollbar at the bottom and use it to scroll the canvas left and right
scrollbar_bottom = ttk.Scrollbar(window, orient='horizontal', command=canvas.xview)
canvas.configure(xscrollcommand=scrollbar_bottom.set)
scrollbar_bottom.place(relx=0, rely=1, relwidth=1, anchor='sw')
# add an event to scroll left / right on Ctrl + mousewheel
canvas.bind('<Control MouseWheel>', lambda event: canvas.xview_scroll(-int(event.delta/60), "units"))

# text box scrollbar
# text = tk.Text(window)
# for i in range(1, 50):
#     text.insert(f'{i}.0', f'text:{i} \n')
# text.pack(expand=True, fill='both')
#
# scrollbar_text = ttk.Scrollbar(window, orient='vertical', command=text.yview)
# text.configure(yscrollcommand=scrollbar_text.set)
# scrollbar_text.place(relx=1, rely=0, relheight=1, anchor='ne')

# treeview scrollbar
table = ttk.Treeview(window, columns=(1, 2), show='headings')
table.heading(1, text='First Name')
table.heading(2, text='Second Name')
first_names = ['Boo', 'Moo', 'Foo', 'Goo', 'Doo', 'Koo', 'Loo']
second_names = ['One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven']
for i in range(100):
    table.insert(parent='', index=tk.END, values=(choice(first_names), choice(second_names)))
table.pack(expand=True, fill='both')

scrollbar_table = ttk.Scrollbar(window, orient='vertical', command=table.yview)
table.configure(yscrollcommand=scrollbar_table.set)
scrollbar_table.place(relx=1, rely=0, relheight=1, anchor='ne')

window.mainloop()
