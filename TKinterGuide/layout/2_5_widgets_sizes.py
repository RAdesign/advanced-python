# about widgets sizing
import tkinter as tk
from tkinter import ttk

# window setup
window = tk.Tk()
window.title('Widgets sizing')
window.geometry('800x640')

# widgets
label1 = ttk.Label(window, text='Label no.1', background='blue')
label2 = ttk.Label(window, text='Label no.2', background='red', width=100)

# layout version 1
# label1.pack()
# label2.pack()

# grid layout version 2
window.columnconfigure((0, 1), weight=1, uniform='a')
window.rowconfigure((0, 1), weight=1, uniform='a')

label1.grid(row=0, column=0)
label2.grid(row=1, column=0, sticky='nsew')

window.mainloop()
