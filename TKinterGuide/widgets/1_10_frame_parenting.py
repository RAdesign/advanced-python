import tkinter as tk
from tkinter import ttk

# window creation
window = tk.Tk()
window.geometry('800x640')
window.title('Frames and Parenting')

# main frame
frame = ttk.Frame(window, width=200, height=200, borderwidth=10, relief=tk.GROOVE)
frame.pack_propagate(False)
frame.pack(side='left')

# master settings
label = ttk.Label(frame, text='Label in the Frame')
label.pack()

button = ttk.Button(frame, text='Button in the Frame')
button.pack()

# example
label2 = ttk.Label(window, text='Label outside the Frame')
label2.pack(side='left')

# example2 - another frame with a label, button, and an entry and put it on the right of other widgets
frame2 = ttk.Frame(window)
ttk.Label(frame2, text='Label in the Frame 2').pack()
ttk.Button(frame2, text='Button in the Frame 2').pack()
ttk.Entry(frame2).pack()
frame2.pack(side='right')

window.mainloop()
