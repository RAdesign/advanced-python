import tkinter as tk
from tkinter import ttk

# window creation
window = tk.Tk()
window.geometry('800x640')
window.title('Menus')

# main menu
m_menu = tk.Menu(window)

# sub menus
file_menu = tk.Menu(m_menu, tearoff=False)
file_menu.add_command(label='New File', command=lambda: print('New File'))
file_menu.add_command(label='Open File', command=lambda: print('Open File'))
file_menu.add_separator()
m_menu.add_cascade(label='File', menu=file_menu)

help_menu = tk.Menu(m_menu, tearoff=False)
help_menu.add_command(label='Help entry', command=lambda: print(help_check_string.get()))

help_check_string = tk.StringVar()
help_menu.add_checkbutton(label='Check', onvalue='on', offvalue='off', variable=help_check_string)

m_menu.add_cascade(label='Help', menu=help_menu)

pizza_menu = tk.Menu(m_menu, tearoff=False)
pizza_menu.add_command(label='Order Pizza', command=lambda: print('Order Pizza'))
pizza_menu.add_command(label='Pay For Pizza', command=lambda: print('Pay For Pizza'))
pizza_menu.add_command(label='Complain on Pizza', command=lambda: print('Complain on Pizza'))
pizza_menu.add_command(label='Get Fat', command=lambda: print('Get Fat'))
m_menu.add_cascade(label='Pizza', menu=pizza_menu)

pasta_menu = tk.Menu(m_menu, tearoff=False)
pasta_menu.add_command(label='Order Pasta', command=lambda: print('Order Pasta'))
pasta_menu.add_command(label='Pay For Pasta', command=lambda: print('Pay For Pasta'))
pasta_menu.add_command(label='Complain on Pasta', command=lambda: print('Complain on Pasta'))
pasta_menu.add_command(label='Get Fat Quicker', command=lambda: print('Get Fat Quicker'))
m_menu.add_cascade(label='Pasta', menu=pasta_menu)

window.configure(menu=m_menu)

# menu as buttons
menu_buttons = ttk.Menubutton(window, text='Menu Button')
menu_buttons.pack()

submenu_button = tk.Menu(menu_buttons, tearoff=False)
submenu_button.add_command(label='Pizza', command=lambda: print('Pizza 1'))
submenu_button.add_checkbutton(label='Check Pizza')
# one of 2 ways to add roll-down menu:
menu_buttons.configure(menu=submenu_button)
# menu_buttons['menu'] = submenu_button


window.mainloop()


