import tkinter as tk
from tkinter import ttk


# setup
window = tk.Tk()
window.title('Buttons')
window.geometry('800x600')

radio_var = tk.StringVar()


# button
def button_func():
    print('a basic button')
    print(radio_var.get())


button_string = tk.StringVar(value='button with string var')
button = ttk.Button(window, text='simple button', command=button_func(), textvariable=button_string)
button.pack()

# checkbutton
check_var = tk.IntVar(value=12)
check1 = ttk.Checkbutton(
    window,
    text='checkbox 1',
    command=lambda: print(check_var.get()),
    variable=check_var,
    onvalue=12,
    offvalue=5
)
check1.pack()

check2 = ttk.Checkbutton(
    window,
    text='checkbox 2',
    command=lambda: print(check_var.get())
)
check2.pack()

# radio buttons

radio1 = ttk.Radiobutton(
    window,
    text='radiobutton 1',
    value=1,
    variable=radio_var,
    command=lambda: print(radio_var.get()),
)
radio1.pack()

radio2 = ttk.Radiobutton(window, text='radiobutton 2', value=1, variable=radio_var)
radio2.pack()

# practice:
# a second radiobutton - values for buttons A and B, ticking either prints value of checkbutton, ticking radio unchecks
# check button - ticking prints value of the radio button value, tkinter var for Bools


def radio_func():
    print(check_bool.get())
    check_bool.set(False)


# data
radio_string = tk.StringVar()
check_bool = tk.BooleanVar()


# widgets
exercise_radio1 = ttk.Radiobutton(
    window,
    text='Radio A',
    value='A',
    command=radio_func,
    variable=radio_string
)
exercise_radio2 = ttk.Radiobutton(
    window,
    text='Radio B',
    value='B',
    command=radio_func,
    variable=radio_string
)
exercise_check = ttk.Checkbutton(
    window,
    text='exercise check',
    variable=check_bool,
    command=lambda: print(radio_string.get())
)

# layout
exercise_radio1.pack()
exercise_radio2.pack()
exercise_check.pack()

#
window.mainloop()
