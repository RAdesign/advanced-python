import tkinter as tk
from tkinter import ttk


def button_func():
    print(string_var.get())
    string_var.set('button pressed')


# make a window
window = tk.Tk()
window.title('Tkinter Variables')
window.geometry('800x600')

# tkinter variable
string_var = tk.StringVar()

# widgets
label = ttk.Label(master=window, text='label1', textvariable=string_var)
label.pack()

# ttk entry
entry = ttk.Entry(master=window, textvariable=string_var)
entry.pack()

# ttk button
button = ttk.Button(master=window, text='the button', command=button_func)
button.pack()


# 2 entry fields and 1 extra label, connected via a StringVar, set a start value of 'test'
exercise_var = tk.StringVar(value='test')
exercise_var.set('test')

entry1 = ttk.Entry(master=window, textvariable=exercise_var)
entry1.pack()
entry2 = ttk.Entry(master=window, textvariable=exercise_var)
entry2.pack()
exercise_label = ttk.Label(master=window, text='label1', textvariable=exercise_var)
exercise_label.pack()
# run
window.mainloop()
