import tkinter as tk
from tkinter import ttk

# setup
window = tk.Tk()
window.geometry('800x600')
window.title('Canvas')

# canvas
canvas = tk.Canvas(window, bg='white')
canvas.pack()


# first pack of canvas function testing
canvas.create_rectangle((50, 20, 100, 200), fill='red', width=10, dash=(4, 2, 1, 1), outline='green')
canvas.create_oval((200, 0, 300, 100), fill='blue')
canvas.create_arc(
    (200, 0, 300, 100),
    fill='red',
    start=45,
    extent=140,
    style=tk.CHORD,
    outline='black',
    width=1
)
canvas.create_line((0, 0, 100, 150), fill='yellow')
canvas.create_polygon((0, 0, 100, 200, 300, 50, 150, -50), fill='grey')
canvas.create_text((100, 200), text='sample text pierogi', fill='pink', width=10)
canvas.create_window((150, 100), window=ttk.Button(window, text='text on canvas pierogi'))


# how to draw in canvas
def draw_on_canvas(event):
    x = event.x
    y = event.y
    canvas.create_oval((x - brush_size/2, y - brush_size/2, x + brush_size/2, y + brush_size/2), fill='black')


# simple brush change sizes
def brush_size_adjustment(event):
    global brush_size
    if event.delta > 0:
        brush_size += 4
    else:
        brush_size -= 4

    brush_size = max(0, min(brush_size, 50))


brush_size = 3
canvas.bind('<Motion>', draw_on_canvas)
canvas.bind('<MouseWheel>', brush_size_adjustment)

window.mainloop()
