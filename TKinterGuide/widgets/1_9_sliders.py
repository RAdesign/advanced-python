import tkinter as tk
from tkinter import ttk
from tkinter import scrolledtext

# window creation
window = tk.Tk()
window.title('Sliders in Tkinter')

# slider
scale_float = tk.DoubleVar(value=15)
scale = ttk.Scale(
    window,
    command=lambda value: progress.stop(),
    from_=0,
    to=25,
    length=400,  # try different lengths for scaling effect
    orient='horizontal',
    variable=scale_float
)
scale.pack()

# create progress bar
progress = ttk.Progressbar(
    window,
    variable=scale_float,
    maximum=25,
    orient='horizontal',
    mode='indeterminate',
    length=400
)
progress.pack()

# test progress
progress.start(1000)

# scrolled text
scrolled_text = scrolledtext.ScrolledText(window, width=100, height=5)
scrolled_text.pack()

# sample - progress vertical, starts automatically, and shows progress as a number
# add scale slider that is affected by the progress bar

sample_int = tk.IntVar()
sample_progress = ttk.Progressbar(window, orient='vertical', variable=sample_int)
sample_progress.pack()
sample_progress.start()

label = ttk.Label(window, textvariable=sample_int)
label.pack()

sample_scale = ttk.Scale(window, variable=sample_int, from_=0, to=100)
sample_scale.pack()

window.mainloop()
