# a series about widgets in TkInter
import tkinter as tk
from tkinter import ttk


def button_func():
    print('this button was pressed')


def exercise_button_func():
    print('hi, Mark !')


# make a window
window = tk.Tk()
window.title('Window and widgets')
window.geometry('800x600')

# ttk label
label = ttk.Label(master=window, text='This is test label')
label.pack()

# tk.text
text = tk.Text(master=window)
text.pack()

# ttk entry
entry = ttk.Entry(master=window)
entry.pack()

# exercise label
exercise_label = ttk.Label(master=window, text='my tiny label')
exercise_label.pack()

# exercise button
exercise_button = ttk.Button(master=window, text='exercise button', command=exercise_button_func)
exercise_button.pack()

# ttk button
button = ttk.Button(master=window, text='a button', command=button_func)
button.pack()

# run
window.mainloop()
