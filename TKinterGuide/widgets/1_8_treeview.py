import tkinter as tk
from tkinter import ttk
from random import choice

# window creation
window = tk.Tk()
window.geometry('800x640')
window.title('Treeview test')

# provide data
first_names = ["Rado", "Miro", "Slobodan", "Maria", "Magdalena", "Henryk", "Wojciech", "Dario", "Mario"]
last_names = ["Van", "Klose", "deHague", "Varia", "Marzena", "The Mad", "The Arab", "Vodka", "Bros"]

# preparing treeview
table = ttk.Treeview(window, columns=('First', 'Last', 'e-mail'), show='headings')
table.heading('First', text='First Name')
table.heading('Last', text='Last Name')
table.heading('e-mail', text='E-mail')
table.pack(fill='both', expand=True)

# inserting values into table
table.insert(parent='', index=0, values=('Jan', 'Serce', 'polskiedrogi@war.pl'))
for i in range(100):
    first = choice(first_names)
    last = choice(last_names)
    email = f'{first[0]}{last}@email.com'
    data = (first, last, email)
    table.insert(parent='', index=0, values=data)

table.insert(parent='', index=tk.END, values=('XXXXX', 'YYYYY', 'ZZZZZ'))


# apply events
def item_select(_):
    print(table.selection())
    for j in table.selection():
        print(table.item(j)['values'])
    # table.item(table.selection())


def delete_items(_):
    print('delete')
    for j in table.selection():
        table.delete(j)


table.bind('<<TreeviewSelect>>', item_select)
table.bind('<Delete>', delete_items)

window.mainloop()
