import tkinter as tk
from tkinter import ttk


# setup
window = tk.Tk()
window.geometry('600x400')
window.title('Combo and Spin')

# combobox
items = ('Paluszki', 'Ziemniaczki', 'Schabowy')
food_string = tk.StringVar(value=items[0])
combo = ttk.Combobox(window, textvariable=food_string)
combo['values'] = items
# combo.configure(values = items)
combo.pack()

# events
combo.bind('<<ComboboxSelected>>', lambda event: combo_label.config(text=f'Selected value: {food_string.get()}'))
combo_label = ttk.Label(window, text='a label')
combo_label.pack()

# spinbox
spin_int = tk.IntVar(value=12)
spin = ttk.Spinbox(
    window,
    from_=3,
    to=20,
    increment=3,
    command=lambda: print(spin_int.get()),
    textvariable=spin_int)
spin.bind('<<Increment>>', lambda event: print('up'))
spin.bind('<<Decrement>>', lambda event: print('down'))
# spin['value'] = (1,2,3,4,5)
spin.pack()

# sample 2
# a spinbox with letters ABCDE
# print the value whenever is decreased (and increased)

sample_letters = ('A', 'B', 'C', 'D', 'E')
sample_string = tk.StringVar(value=sample_letters[0])
sample_spin = ttk.Spinbox(window, textvariable=sample_string, values=sample_letters)
sample_spin.pack()

sample_spin.bind('<<Increment>>', lambda event: print(sample_string.get()))
sample_spin.bind('<<Decrement>>', lambda event: print(sample_string.get()))

window.mainloop()
