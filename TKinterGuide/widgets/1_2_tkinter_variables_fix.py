import tkinter as tk
from tkinter import ttk


# make a window
window = tk.Tk()
window.title('Tkinter Variables')
window.geometry('800x600')

# tkinter variable
string_var = tk.StringVar()

# widgets
label = ttk.Label(master=window, text='label', textvariable=string_var)
label.pack()

# ttk entry
entry = ttk.Entry(master=window, textvariable=string_var)
entry.pack()

entry2 = ttk.Entry(master=window, textvariable=string_var)
entry2.pack()

label2 = ttk.Label(master=window, text='label2', textvariable=string_var)
label2.pack()

entry3 = ttk.Entry(master=window, textvariable=string_var)
entry3.pack()

window.mainloop()
