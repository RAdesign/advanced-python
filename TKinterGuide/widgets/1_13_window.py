import tkinter as tk
from tkinter import ttk

# window creation
window = tk.Tk()
window.geometry('800x640+80+160')
window.title('Windows TkI')
window.iconbitmap('RA.ico')

# window in a middle of a screen
window_width = 1200
window_height = 680
display_width = window.winfo_screenwidth()
display_height = window.winfo_screenheight()

left = int(display_width/2 - window_width/2)
top = int(display_height/2 - window_height/2)
window.geometry(f'{window_width}x{window_height}+{left}+{top}')

# window sizes
window.minsize(240, 180)
# window.maxsize(1600, 1200)
window.resizable(True, True)

# screen attributes
print(window.winfo_screenwidth())
print(window.winfo_screenheight())

# window attributes
window.attributes('-alpha', 1)
# do not run this! window.attributes('-topmost', True)

# security event
window.bind('<Escape>', lambda event: window.quit())

# window.attributes('-disable', True)
# do not use this! window.attributes('-fullscreen', True)

# title bar
window.overrideredirect(False)  # True destroys
grip = ttk.Sizegrip(window)
grip.place(relx=1.0, rely=1.0, anchor='se')

window.mainloop()
