import tkinter as tk
from tkinter import ttk

# window creation
window = tk.Tk()
window.geometry('800x640')
window.title('Tabs Widget')

# a notebook widget
notebook = ttk.Notebook(window)

# tab no.1
tab1 = ttk.Frame(notebook)
label1 = ttk.Label(tab1, text='Tab 1 Text')
label1.pack()
button1 = ttk.Button(tab1, text='Tab 1 Button')
button1.pack()

# tab no.2
tab2 = ttk.Frame(notebook)
label2 = ttk.Label(tab2, text='Tab 1 Text')
label2.pack()
button2 = ttk.Entry(tab2)
button2.pack()

notebook.add(tab1, text='Tab 1')
notebook.add(tab2, text='Tab 2')
notebook.pack()

# extra - another tab with 2 buttons and one label inside
tab_extra = ttk.Frame(notebook)
button_extra_1 = ttk.Button(tab_extra, text='Button E1')
button_extra_1.pack()

button_extra_2 = ttk.Button(tab_extra, text='Button E2')
button_extra_2.pack()

label_extra_1 = ttk.Label(tab_extra, text='Label E1')
label_extra_1.pack()

notebook.add(tab_extra, text='Extra Tab')

window.mainloop()
