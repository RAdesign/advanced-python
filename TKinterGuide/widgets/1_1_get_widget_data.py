import tkinter as tk
from tkinter import ttk


def button_func():
    # get the content of an entry
    entry_text = entry.get()
    print('this button was pressed')

    # update the label
    # label.configure(text='some other text')
    label['text'] = entry_text
    entry['state'] = 'disabled'
    # print(label.configure())


# make a window
window = tk.Tk()
window.title('Getting and setting widgets')
window.geometry('800x600')

# widgets
label = ttk.Label(master=window, text='Widget text')
label.pack()

# ttk entry
entry = ttk.Entry(master=window)
entry.pack()

# ttk button
button = ttk.Button(master=window, text='the button', command=button_func)
button.pack()


# button changing text on label
def reset_func():
    label['text'] = 'new text'
    entry['state'] = 'enabled'


# exercise button
exercise_button = ttk.Button(master=window, text='exercise button', command=reset_func)
exercise_button.pack()

# run
window.mainloop()
