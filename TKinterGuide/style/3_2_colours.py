import tkinter as tk
from tkinter import ttk

# window creation
window = tk.Tk()
window.title('Colours')
window.geometry('800x600')

# widgets creation
ttk.Label(window, background='red').pack(expand=True, fill='both')
ttk.Label(window, background='#08F').pack(expand=True, fill='both')
ttk.Label(window, background='#4fc296').pack(expand=True, fill='both')

# white and black
ttk.Label(window, background='#000').pack(expand=True, fill='both')
ttk.Label(window, background='#888').pack(expand=True, fill='both')
ttk.Label(window, background='#FFF').pack(expand=True, fill='both')

window.mainloop()
