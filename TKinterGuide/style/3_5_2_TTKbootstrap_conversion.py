import tkinter as tk
# from tkinter import ttk
import ttkbootstrap as ttk

# check for the bootstrap themes availability


class App(ttk.Window):
    def __init__(self, title, size):
        # main setup
        super().__init__(themename='cosmo')
        self.title(title)
        self.geometry(f'{size[0]}x{size[1]}')
        self.minsize(size[0], size[1])
        # widgets
        self.menu = Menu(self)
        self.main = Main(self)
        # main loop
        self.mainloop()


class Menu(ttk.Frame):
    def __init__(self, parent):
        super().__init__(parent)
        self.place(x=0, y=0, relwidth=0.3, relheight=1)
        self.create_widgets()

    def create_widgets(self):
        # create widgets
        menu_button_1 = ttk.Button(self, text='Button 1S', style='danger')
        menu_button_2 = ttk.Button(self, text='Button 2S', style='success')
        menu_button_3 = ttk.Button(self, text='Button 3S', style='dark')

        menu_slider_1 = ttk.Scale(self, orient='vertical', style='info')
        menu_slider_2 = ttk.Scale(self, orient='vertical', style='secondary')

        toggle_frame = ttk.Frame(self)
        menu_toggle_1 = ttk.Checkbutton(toggle_frame, text='Check 1', style='info')
        menu_toggle_2 = ttk.Checkbutton(toggle_frame, text='Check 1')

        entry = ttk.Entry(self)

        # grid creation
        self.columnconfigure((0, 1, 2), weight=1, uniform='a')
        self.rowconfigure((0, 1, 2, 3, 4), weight=1, uniform='a')

        # placing widgets
        menu_button_1.grid(row=0, column=0, sticky='nswe', columnspan=2, padx=4, pady=4)
        menu_button_2.grid(row=0, column=2, sticky='nswe', padx=4, pady=4)
        menu_button_3.grid(row=1, column=0, sticky='nsew', columnspan=3, padx=4, pady=4)

        menu_slider_1.grid(row=2, column=0, sticky='nsew', rowspan=2, pady=20)
        menu_slider_2.grid(row=2, column=2, sticky='nsew', rowspan=2, pady=20)

        # toggling layout
        toggle_frame.grid(row=4, column=0, columnspan=3, sticky='nsew')
        menu_toggle_1.pack(side='left', expand=True)
        menu_toggle_2.pack(side='left', expand=True)

        # entry layout
        entry.place(relx=0.5, rely=0.95, relwidth=0.9, anchor='center')


class Main(ttk.Frame):
    def __init__(self, parent):
        super().__init__(parent)
        self.place(relx=0.3, y=0, relwidth=0.7, relheight=1)
        Entry(self, 'Entry 1 Entry Class', 'Button 1 Entry Class', 'green')
        Entry(self, 'Entry 2 Entry Class', 'Button 2 Entry Class', 'blue')


class Entry(ttk.Frame):
    def __init__(self, parent, label_text, button_text, label_background):
        super().__init__(parent)
        label = ttk.Label(self, text=label_text, background=label_background)
        button = ttk.Button(self, text=button_text)
        label.pack(expand=True, fill='both')
        button.pack(expand=True, fill='both')
        self.pack(side='left', expand=True, fill='both', padx=20, pady=20)


App('Class based Application', (600, 600))
