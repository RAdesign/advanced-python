# import tkinter as tk
# from tkinter import ttk
import ttkbootstrap as ttk

window = ttk.Window(themename='journal')
window.title('TTK bootstrap basics')
window.geometry('800x600')

label = ttk.Label(window, text='Label')
label.pack(pady=10)

button_1 = ttk.Button(window, text='Red', style='danger-outline')
button_1.pack(pady=10)

button_2 = ttk.Button(window, text='Attention!', style='warning')
button_2.pack(pady=10)

button_3 = ttk.Button(window, text='Green', style='success')
button_3.pack(pady=10)

window.mainloop()
