import customtkinter as ctk
import tkinter as tk
from tkinter import ttk

# window creation
window = ctk.CTk()
window.title('Custom TKinter App')
window.geometry('800x640')

# widgets creation
string_var = ctk.StringVar(value='custom string')
label = ctk.CTkLabel(
    window,
    text='CTk Label',
    fg_color=('blue', 'red'),
    text_color=('black', 'white'),
    corner_radius=10,
    textvariable=string_var)
label.pack()

button = ctk.CTkButton(
    window,
    text='CTk Button',
    fg_color='#FF0',
    text_color='#000',
    hover_color='#AA0',
    command=lambda: ctk.set_appearance_mode('light'))

frame = ctk.CTkFrame(window)
frame.pack()

slider = ctk.CTkSlider(frame)
slider.pack(padx=20, pady=20)

# extra
switch = ctk.CTkSwitch(
    window,
    text='Exercise Switch',
    fg_color=('blue', 'red'),
    progress_color='pink',
    button_color='green',
    button_hover_color='yellow',
    switch_width=60,
    switch_height=30,
    corner_radius=2)
switch.pack()

window.mainloop()
