# styling in TKinter, basics
import tkinter as tk
from tkinter import ttk, font

# window creation
window = tk.Tk()
window.title('Styling')
window.geometry('800x600')

# check out the font families
print(font.families())

# styles
style = ttk.Style()
print(style.theme_names())
style.theme_use('classic')

style.configure('new.TButton', foreground='green', font=('Jokerman', 30))
style.map('new.TButton',
          foreground=[('pressed', 'red'), ('disabled', 'yellow')],
          background=[('pressed', 'green'), ('active', 'blue')]
          )
style.configure('TFrame', background='pink')

# widgets
label = ttk.Label(
    window,
    text='Label line\nAnother line',
    background='red',
    foreground='white',
    font=('Jokerman', 20),
    justify='right'
)
label.pack()

button = ttk.Button(window, text='A button', style='new.TButton')
button.pack()

# another one - add a frame with width and height, with pink bg colour
frame = ttk.Frame(window, height=200, width=100)
frame.pack()

window.mainloop()
