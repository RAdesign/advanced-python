"""Simple calculations definitions for testing with unittest.
Ad hoc testing could be done just by using print, or logging, but it`s better to use testing modules"""


def add(x, y):
    # Addition
    return x + y


def subtract(x, y):
    # Subtraction
    return x - y


def multiply(x, y):
    # Multiplication
    return x * y


def divide(x, y):
    # Division
    if y is 0:
        raise ValueError("Can`t divide by zero")
    return x / y
