"""An employee class fot unittest examples testing"""

import requests


class Employee:
    RAISE_AMOUNT: float = 1.05

    def __init__(self, firstname, lastname, wage):
        self.firstname: str = firstname
        self.lastname: str = lastname
        self.wage: float = wage

    @property
    def email(self):
        return f"{self.firstname}.{self.lastname}@email.com"

    @property
    def fullname(self):
        return f"{self.firstname} {self.lastname}"

    def wage_raise_request(self):
        self.wage = float(self.wage * self.RAISE_AMOUNT)

    def monthly_schedule(self, month):
        response = requests.get(f"http://thisCompany.de/{self.lastname}/{month}")
        if response.ok:
            return response.text
        else:
            return "Wrong response"
