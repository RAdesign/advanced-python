"""using unittest module to test calculations' module.
run like: python -m unittest calculations_test.py, or write if name == main and run this directly"""
import unittest
import calculations  # import module we want to test


# test class for creating test cases, inheriting from unittest to access unittest methods
class TestCalculations(unittest.TestCase):

    def test_add(self):  # first test case, many assert statements can be used (use documentation)
        self.assertEqual(calculations.add(20, 10), 30)
        self.assertEqual(calculations.add(-7, 7), 0)  # try checking for many edge cases
        self.assertEqual(calculations.add(-14, -7), -21)

    def test_subtract(self):
        self.assertEqual(calculations.subtract(20, 10), 10)
        self.assertEqual(calculations.subtract(-7, 7), -14)
        self.assertEqual(calculations.subtract(-14, -7), -7)

    def test_multiply(self):
        self.assertEqual(calculations.multiply(20, 10), 200)
        self.assertEqual(calculations.multiply(-7, 7), -49)
        self.assertEqual(calculations.multiply(-14, -7), 98)

    def test_divide(self):
        self.assertEqual(calculations.divide(20, 10), 2)
        self.assertEqual(calculations.divide(-7, 7), -1)
        self.assertEqual(calculations.divide(-14, -7), 2)

        with self.assertRaises(ValueError):  # using context manager is more proper way to test exceptions
            calculations.divide(20, 0)


if __name__ == "__main__":
    unittest.main()
