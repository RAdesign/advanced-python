"""unit testing of employee class wit unittest module.
Tests should be independent of each other."""

import unittest
from unittest.mock import patch
from employee import Employee


class TestEmployee(unittest.TestCase):

    # classmethod allows for general setUp and tearDown for entire class consists of instances of test cases
    # a good use case will be to populate a database for test class before running instances of test cases
    # or to do a cleanup with tearDown at the end.
    @classmethod
    def setUpClass(cls):
        print("setupClass")

    @classmethod
    def tearDownClass(cls):
        print("tearDownClass")

    def setUp(self):  # employee creation definition to stay DRY
        print("setUp\n")
        self.employee_1 = Employee("John", "Threevoltage", 50000)  # employee is an instance attribute of setUp
        self.employee_2 = Employee("Karen", "Barren", 60000)

    def tearDown(self):
        print("tearDown\n")  # tearDown is for testing deletion, removal - files, directories, objects, etc

    def test_email(self):
        print("testing_email")  # checking email creation
        self.assertEqual(self.employee_1.email, "John.Threevoltage@email.com")
        self.assertEqual(self.employee_2.email, "Karen.Barren@email.com")

        self.employee_1.firstname = "Jane"  # check if works with changes
        self.employee_2.lastname = "Mirror"

        self.assertEqual(self.employee_1.email, "Jane.Threevoltage@email.com")
        self.assertEqual(self.employee_2.email, "Karen.Mirror@email.com")

    def test_fullname(self):
        print("testing_fullname")  # checking fullname creation
        self.assertEqual(self.employee_1.fullname, "John Threevoltage")
        self.assertEqual(self.employee_2.fullname, "Karen Barren")

        self.employee_1.firstname = "Jane"  # checking if it works with changes
        self.employee_2.lastname = "Mirror"

        self.assertEqual(self.employee_1.fullname, "Jane Threevoltage")
        self.assertEqual(self.employee_2.fullname, "Karen Mirror")

    def test_wage_raise(self):  # checking if changing wages works
        print("testing_wage_raise")
        self.employee_1.wage_raise_request()
        self.employee_2.wage_raise_request()

        self.assertEqual(self.employee_1.wage, 52500)
        self.assertEqual(self.employee_2.wage, 63000)

    def test_monthly_schedule(self):
        # a Python Mock, here used to be able to test offline or with website down
        # using Patch as a context manager, within Patch is what we want to mock, we want to mock things where
        # they`re actually used - here within object
        with patch("employee.requests.get") as mocked_get:
            mocked_get.return_value.ok = True  # set response as true
            mocked_get.return_value.text = "Success"  # if response true - we get this message

            schedule = self.employee_1.monthly_schedule("June")  # requests a schedule for employee from website
            mocked_get.assert_called_with("http://thisCompany.de/Threevoltage/June")  # assert if called w/proper value
            self.assertEqual(schedule, "Success")  # assert if ok response message is 'success'

            mocked_get.return_value.ok = False

            schedule = self.employee_2.monthly_schedule("August")
            mocked_get.assert_called_with("http://thisCompany.de/Barren/August")
            self.assertEqual(schedule, "Wrong response")


if __name__ == "__main__":
    unittest.main()
