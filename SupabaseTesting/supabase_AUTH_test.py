import os
from dotenv import load_dotenv
from supabase import create_client, Client
from gotrue.errors import AuthApiError


load_dotenv()
url: str = os.environ.get("SUPABASE_URL")
key: str = os.environ.get("SUPABASE_KEY")
supabase: Client = create_client(url, key)

# using GitHub of supabase community
random_email: str = "3hf82fijf92@supamail.com"
random_password: str = "fqj13bnf2hiu23h"
user = supabase.auth.sign_up(random_email=random_email, password=random_password)

"""# use own email to test
email: str = "__"
password: str = "__"
user = supabase.auth.sign_up(email=email, password=password)"""

# create a session and call for user data in that session
try:
    session = supabase.auth.sign_in_with_password(email=random_email, password=random_password)
    print(session.user)
except AuthApiError:
    print("Login failed")

supabase.auth.sign_out()
