import os
from dotenv import load_dotenv
from datetime import datetime, timedelta
from supabase import create_client, Client

load_dotenv()
url: str = os.environ.get("SUPABASE_URL")
key: str = os.environ.get("SUPABASE_KEY")
supabase: Client = create_client(url, key)

# RLS - Row Level Security is disabled for this testing
# for RLS use private keys from bash snippets in supabase


# for inserting creation time
created_at = datetime.utcnow() - timedelta(hours=-1)

# use rich supabase API documentation
"""response1 = supabase.table('TODOs').select("id, item_name").execute()
print(response1)"""
# using a filter
"""response2 = supabase.table('TODOs').select("id, item_name").eq("name", "item_2").execute()
print(response2)"""

# data creation
"""data = supabase.table('TODOs').insert({"item_name": "task_2"}).execute()
response1 = supabase.table('TODOs').select("*").execute()
print(response1)"""

# data creation with creation time
"""data2 = supabase.table('TODOs').insert({"item_name": "task_3", "created_at": str(created_at)}).execute()
response3 = supabase.table('TODOs').select("*").execute()
print(response3)"""

# updating data
# data3 = supabase.table('TODOs').update({"item_name": "updated_task"}).eq("id", 1).execute()

# deleting data
"""data4 = supabase.table('TODOs').delete().eq("id", 2).execute()
response4 = supabase.table('TODOs').select("*").execute()
print(response4)"""
