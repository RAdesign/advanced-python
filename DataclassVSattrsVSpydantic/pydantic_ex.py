"""pydantic is based on 'attrs' package, pydantic excels in validation - a lot of built-in validators: email,
credit card number, and many more, but it`s harder to modify comparisons between objects.
Pydantic works well with FastAPI, more strict than `attrs'. Cons: external dependency, inheritance mechanism is
not that good with composition, string dunder method does not work with actual class"""
from datetime import date
from enum import StrEnum, auto

from pydantic import BaseModel, Field, PositiveFloat, PositiveInt


class OrderStatus(StrEnum):
    OPEN = auto()
    CLOSED = auto()


class Product(BaseModel):  # BaseModel - pydantic uses inheritance relationship instead of decorators
    name: str
    category: str
    shipping_weight: PositiveFloat  # built-in validator type of pydantic
    unit_price: PositiveInt # built-in validator type of pydantic
    tax_value: float = Field(ge=0, le=1)  # here a Field validation


class Order(BaseModel):
    status: OrderStatus
    creation_date: date = date.today()
    products: list[Product] = Field(default_factory=list)  # same Field as in attrs

    def add_product(self, product: Product) -> None:
        self.products.append(product)

    @property
    def sub_total(self) -> int:
        return sum((p.unit_price for p in self.products))

    @property
    def taxation(self) -> float:
        return sum(product.unit_price * product.tax_value for product in self.products)

    @property
    def total_price(self) -> float:
        return self.sub_total + self.taxation

    @property
    def total_shipping_weight(self) -> float:
        return sum(product.shipping_weight for product in self.products)


def main() -> None:
    apple = Product(
        name="apple",
        category="fruit",
        shipping_weight=0.22,
        unit_price=5,
        tax_value=0.01,
    )

    pear = Product(
        name="pear",
        category="fruit",
        shipping_weight=0.25,
        unit_price=7,
        tax_value=0.01,
    )

    premium_pear = Product(
        name="Pear",
        category="Fruit",
        shipping_weight=0.3,
        unit_price=11,
        tax_value=0.02,
    )

    order = Order(status=OrderStatus.OPEN)
    for product in [apple, pear, premium_pear]:
        order.add_product(product)

    print(f"Compare pear and premium pear: {pear == premium_pear}")
    print(f"Total order price: {order.total_price / 100:.2f}")
    print(f"Subtotal order price: {order.sub_total / 100:.2f}")
    print(f"Taxation vale: {order.taxation / 100:.2f}")
    print(f"Total order weight: {order.total_shipping_weight} kg")


if __name__ == "__main__":
    main()
