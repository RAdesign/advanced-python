"""Reading data and its validation , using Pydantic module"""

import json
from typing import List, Optional
import pydantic


# Exception for ISBN10 and ISBN13 missing
class ISBNMissingError(Exception):
    def __init__(self, title: str, message: str) -> None:
        self.title = title
        self.message = message
        super().__init__(message)


# Exception when ISBN10 has wrong format
class ISBN10FormatError(Exception):
    def __init__(self, value: str, message: str) -> None:
        self.value = value
        self.message = message
        super().__init__(message)


class Author(pydantic.BaseModel):
    name: str
    verified: bool


# a Pydantic inherited class representing a book, readable from JSON file
class Book(pydantic.BaseModel):
    title: str
    author: str
    publisher: str
    price: float
    isbn_10: Optional[str]  # Optional as every that field is optional
    isbn_13: Optional[str]
    subtitle: Optional[str]
    author_2: Optional[Author]

    # checks whether is there any ISBN value. It`s validating the entire model, not just one field
    # Therefore - use of root_validator, and not just validator
    @pydantic.root_validator(pre=True) # pre - when it validates - before of after data conversion. True - before.
    @classmethod
    def check_isbn_10_or_13(cls, values):
        if "isbn_10" not in values and "isbn_13" not in values:
            raise ISBNMissingError(
                title=values["title"],
                message="Document lacks either ISBN10 or ISBN13",
            )
        return values  # result of root validator will be a list of values

    # Pydantic validator. Check if ISBN10 is valid
    @pydantic.validator("isbn_10")  # Pydantic validator decorator, it`s a class method
    @classmethod  # not strictly needed to add this, but helps to prevent some problems
    def isbn_10_valid(cls, value) -> None:
        chars = [char_item for char_item in value if char_item in "0123456789Xx"]  # value into list
        if len(chars) != 10:  # check if list contains valid number of characters
            raise ISBN10FormatError(
                value=value,
                message="ISBN10 format has to have 10 characters",
            )

        def char_to_int(char: str) -> int:  # this function is nested, as it`s only place where it`s used
            if char in "Xx":
                return 10
            return int(char)

        # weighted sum of ISBN numbers, to confirm its validation
        if sum((10 - indx) * char_to_int(x) for indx, x in enumerate(chars)) % 11 != 0:
            raise ISBN10FormatError(
                value=value,
                message="ISBN10 digit sum must be dividable by 11"
            )
        return value

    class Config:  # Pydantic config class, some examples
        allow_mutation = False  # for creating immutable objects. Now 'books' are immutable objects
        anystr_lower = False  # converts all to lowercase


def main():
    with open("./books.json") as file:
        data = json.load(file)
        books: List[Book] = [Book(**item) for item in data]  # putting data in a list and doing list comprehension
        # print(books)  # print all
        print(books[1].title)  # print attribute
        print(books[1].dict(exclude={"price"}))  # converts data to a python dict type, allows for dict methods
        #print(books[1].copy())  # w/'deep=True' flag allows for deep copy, for more complex databases


if __name__ == "__main__":
    main()
