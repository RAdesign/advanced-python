"""attrs attributes advantages example, 'attrs' library is much more extensive than dataclasses, validation
is done better than in dataclasses , but worse than in pydantic. Cons: 'attrs' is external dependency,
version problem."""
from __future__ import annotations
from datetime import date
from enum import StrEnum, auto   # StrEnum problems with python 3.10, works with 3.11

from attrs import Attribute, define, field, validators


class OrderStatus(StrEnum):  # automatically generates string from Enum
    OPEN = auto()
    CLOSED = auto()


def positive_number(instance: type, attribute: Attribute[str], value: int | float) -> None:
    """Extra test if an attribute of an instance was assigned a positive value"""
    class_name = instance.__class__.__name__
    if value <= 0:
        raise ValueError(f"Class: {class_name} attribute: {attribute} must be greater than zero ")


def precentage_value(instance: type, attribute: Attribute[str], value: float) -> None:
    # Extra test if an attribute of an instance was assigned a precentage
    class_name = instance.__class__.__name__
    if not 0 <= value <= 1:
        raise ValueError(f"Class: {class_name} attribute: {attribute} must be between 0 and 1 ")


@define  # define decorator from 'attrs'
class Product:
    name: str = field(eq=str.lower)  # comparison of name and category - more extensive in attrs than dataclasses
    category: str = field(eq=str.lower)  # in attrs we have control of how things are compared
    # validators available in 'attrs':
    shipping_weight: float = field(validator=[validators.instance_of(float), positive_number], eq=False)
    unit_price: float = field(validator=[validators.instance_of(float), positive_number], eq=False)
    tax_value: float = field(validator=[validators.instance_of(float), precentage_value], eq=False)

    def __post_init__(self) -> None:  # how validation is applied
        if self.unit_price < 0:
            raise ValueError("Unit price must be greater than zero")
        if self.shipping_weight < 0:
            raise ValueError("Shipping weight must be greater than zero")
        if not 0 < self.tax_value < 1:
            raise ValueError("Tax value must be between zero and one")


@define(kw_only=True)
class Order:
    status: OrderStatus
    creation_date: date = date.today()
    products: list[Product] = field(factory=list)

    def add_product(self, product: Product) -> None:
        self.products.append(product)

    @property
    def sub_total(self) -> int:
        return sum((p.unit_price for p in self.products))

    @property
    def taxation(self) -> float:
        return sum(product.unit_price * product.tax_value for product in self.products)

    @property
    def total_price(self) -> float:
        return self.sub_total + self.taxation

    @property
    def total_shipping_weight(self) -> float:
        return sum(product.shipping_weight for product in self.products)


def main() -> None:
    apple = Product(
        name="apple",
        category="fruit",
        shipping_weight=0.22,
        unit_price=0.5,
        tax_value=0.01,
    )

    pear = Product(
        name="pear",
        category="fruit",
        shipping_weight=0.25,
        unit_price=0.7,
        tax_value=0.01,
    )

    premium_pear = Product(
        name="Pear",
        category="Fruit",
        shipping_weight=0.3,
        unit_price=1.0,
        tax_value=0.02,
    )

    order = Order(status=OrderStatus.OPEN)
    for product in [apple, pear, premium_pear]:
        order.add_product(product)

    print(f"Compare pear and premium pear: {pear == premium_pear}")
    print(f"Total order price: {order.total_price / 100:.2f}")
    print(f"Subtotal order price: {order.sub_total / 100:.2f}")
    print(f"Taxation vale: {order.taxation / 100:.2f}")
    print(f"Total order weight: {order.total_shipping_weight} kg")


if __name__ == "__main__":
    main()
