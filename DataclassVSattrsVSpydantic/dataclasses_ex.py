"""dataclasses advantages example. Dataclasses are not that good in data validation,
 and converting between data types. However, dataclass in an official library, so has better support.
 But also version of this library is tied to python interpreter version """
from dataclasses import dataclass, field
from datetime import date
from enum import StrEnum, auto


class OrderStatus(StrEnum):
    OPEN = auto()
    CLOSED = auto()


@dataclass
class Product:  # compares types for validation
    name: str = field(compare=True)
    category: str = field(compare=True)
    shipping_weight: float = field(compare=False) # to be able to compare change value
    unit_price: float = field(compare=False)   # int is better for monetary amounts than float, as makes fewer problems
    # so basically one does count 'cents' or any lowest denominator, and then converts to proper currency values
    tax_value: float = field(compare=False)

    def __post_init__(self) -> None:
        if self.unit_price < 0:
            raise ValueError("Unit price must be greater than zero")
        if self.shipping_weight < 0:
            raise ValueError("Shipping weight must be greater than zero")
        if not 0 < self.tax_value < 1:
            raise ValueError("Tax value must be between zero and one")


@dataclass
class Order:
    status: OrderStatus
    creation_date: date = date.today()
    products: list[Product] = field(default_factory=list)

    def add_product(self, product: Product) -> None:
        self.products.append(product)

    @property
    def sub_total(self) -> int:
        return sum((p.unit_price for p in self.products))

    @property
    def taxation(self) -> float:
        return sum(product.unit_price * product.tax_value for product in self.products)

    @property
    def total_price(self) -> float:
        return self.sub_total + self.taxation

    @property
    def total_shipping_weight(self) -> float:
        return sum(product.shipping_weight for product in self.products)


def main() -> None:
    apple = Product(
        name="apple",
        category="fruit",
        shipping_weight=0.22,
        unit_price=0.5,
        tax_value=0.01,
    )

    pear = Product(
        name="pear",
        category="fruit",
        shipping_weight=0.25,
        unit_price=0.7,
        tax_value=0.01,
    )

    premium_pear = Product(
        name="Pear",
        category="Fruit",
        shipping_weight=0.3,
        unit_price=1,
        tax_value=0.02,
    )

    order = Order(status=OrderStatus.OPEN)
    for product in [apple, pear, premium_pear]:
        order.add_product(product)

    print(f"Compare pear and premium pear: {pear == premium_pear}")  # false as different naming case
    print(f"Total order price: {order.total_price/100:.2f}")
    print(f"Subtotal order price: {order.sub_total/100:.2f}")
    print(f"Taxation vale: {order.taxation/100:.2f}")
    print(f"Total order weight: {order.total_shipping_weight} kg")


if __name__ == "__main__":
    main()
    