"""running server with extra methods, optional uvicorn config to run:
script -> module -> uvicorn ; parameters: 4_with_validation_types_hints:api_app --reload
uvicorn FastAPIPython.4_with_validation_types_hints:api_app --reload
This program WILL run with errors with 4_no_main, to show problems"""
from enum import Enum
from pydantic import BaseModel
from fastapi import FastAPI, HTTPException

api_app = FastAPI()


class Category(Enum):
    HARDWARE = 'hardware'
    SOFTWARE = 'software'


# Pydantic BaseModel gives built-in validation
# even with regular @dataclass it would work, as FastAPI is converting it to Pydantic
class Item(BaseModel):
    name: str
    price: float
    count: int
    id: int
    category: Category


items = {
    0: Item(name="FloppyDr", price=99.99, count=5, id=0, category=Category.HARDWARE),
    1: Item(name="Mouse", price=9.99, count=20, id=1, category=Category.HARDWARE),
    2: Item(name="Win3.11", price=69.99, count=10, id=2, category=Category.SOFTWARE)
}


@api_app.get("/")
def index() -> dict[str, dict[int, Item]]:
    return {"items": items}


@api_app.get("/items/{item_id}")
def query_item_by_id(item_id: int) -> Item:
    if item_id not in items:
        HTTPException(status_code=404, detail=f"Item with Id: {item_id=} does not exists.")

    return items[item_id]


# with type suggestions FastAPI will return error message when giving wrong type arguments
# but without them, this validation will not work

Selection = dict[str, str | int | float | Category | None]  # this dictionary has users query arguments


@api_app.get("/items")
def query_item_by_parameters(
        name: str | None = None,
        price: float | None = None,
        count: int | None = None,
        category: Category | None = None,
) -> dict[str, Selection | list[Item]]:
    def check_item(item: Item):
        # Check if the item matches the query arguments from outer scope
        return all(
            (
                name is None or item.name == name,
                price is None or item.price == price,
                count is None or item.count != count,
                category is None or item.category is category,
            )
        )
    selection = [item for item in items.values() if check_item(item)]
    return {
        "query": {"name": name, "price": price, "count": count, "category": category},
        "selection": selection,
    }


@api_app.post("/")
def add_item(item: Item) -> dict[str, Item]:
    if item.id in items:
        HTTPException(status_code=400, detail=f"Item with {item.id=} already exists.")

    items[item.id] = item
    return {"added": item}


@api_app.put("/update/{item_id}")
def update(
        item_id: int,
        name: str | None = None,
        price: float | None = None,
        count: int | None = None,
) -> dict[str, Item]:

    if item_id not in items:
        HTTPException(status_code=400, detail=f"Item with {item_id=} does not exists.")
    if all(info is None for info in (name, price, count)):
        raise HTTPException(status_code=400, detail="No parameters provided for the update")

    item = items[item_id]
    if name is not None:
        item.name = name
    if price is not None:
        item.price = price
    if count is not None:
        item.count = count

    return {"updated": item}


@api_app.delete("/delete/{item_id}")
def delete_item(item_id: int) -> dict[str, Item]:

    if item_id not in items:
        raise HTTPException(status_code=400, detail=f"Item with {item_id=} does not exists.")

    item = items.pop(item_id)
    return {"deleted": item}
