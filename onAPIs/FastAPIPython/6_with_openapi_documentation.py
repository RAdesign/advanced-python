"""running server with extra methods, optional uvicorn config to run:
script -> module -> uvicorn ; parameters: 6_with_openapi_documentation:api_app --reload
uvicorn FastAPIPython.6_with_openapi_documentation:api_app --reload
5_no_main will run and show errors, shown in detail messages
*Alternative is GraphQL"""
from enum import Enum
from pydantic import BaseModel, Field
from fastapi import FastAPI, HTTPException, Path, Query


# API can be extended with additional metadata, as description, version no, etc.
# description supports mark-down formatting
api_app = FastAPI(
    title="90s Computer Shop",
    description="Feel the vibe of a classic 90s computer shop!",
    version="0.8.1",
)


# docstrings of classes will be shown in API documentation in "Schemas" section
class Category(Enum):
    """Items categories"""
    HARDWARE = 'hardware'
    SOFTWARE = 'software'


# adding metadata to attributes with Field class, and it will be shown in auto-generated documentation
class Item(BaseModel):
    """How items are represented in the model"""
    name: str = Field(description="Name of the item.")
    price: float = Field(description="Price of an item in 'currency'.")
    count: int = Field(description="Amount of items in stock.")
    id: int = Field(description="Identification number of the item.")
    category: Category = Field(description="Items Category.")


items = {
    0: Item(name="FloppyDr", price=99.99, count=5, id=0, category=Category.HARDWARE),
    1: Item(name="Mouse", price=9.99, count=20, id=1, category=Category.HARDWARE),
    2: Item(name="Win3.11", price=69.99, count=10, id=2, category=Category.SOFTWARE)
}


@api_app.get("/")
def index() -> dict[str, dict[int, Item]]:
    return {"items": items}


@api_app.get("/items/{item_id}")
def query_item_by_id(item_id: int) -> Item:
    if item_id not in items:
        HTTPException(status_code=404, detail=f"Item with Id: {item_id=} does not exists.")

    return items[item_id]


Selection = dict[str, str | int | float | Category | None]


@api_app.get("/items")
def query_item_by_parameters(
        name: str | None = None,
        price: float | None = None,
        count: int | None = None,
        category: Category | None = None,
) -> dict[str, Selection | list[Item]]:
    def check_item(item: Item):
        # Check if the item matches the query arguments from outer scope
        return all(
            (
                name is None or item.name == name,
                price is None or item.price == price,
                count is None or item.count != count,
                category is None or item.category is category,
            )
        )
    selection = [item for item in items.values() if check_item(item)]
    return {
        "query": {"name": name, "price": price, "count": count, "category": category},
        "selection": selection,
    }


@api_app.post("/")
def add_item(item: Item) -> dict[str, Item]:
    if item.id in items:
        HTTPException(status_code=400, detail=f"Item with {item.id=} already exists.")

    items[item.id] = item
    return {"added": item}


# 'response' keyword is to specify a response a user gets at the endpoint
@api_app.put("/update/{item_id}",
             responses={
                 404: {"description": "Item not found."},
                 400: {"description": "No query arguments specified."},
             },
             )
# Query and Path classes allow to add documentation to the query with parameters
def update(
        item_id: int = Path(title="Item ID", description="Unique int for each item", ge=0),
        name: str | None = Query(
            title="Name", decription="New name of the item", default=None, min_length=1, max_length=8),
        price: float | None = Query(
            title="Price", decription="Price of the item in 'currency'", default=None, gt=0.0),
        count: int | None = Query(
            title="Count", decription="Amount of the item in stock", default=None, gt=0),
) -> dict[str, Item]:

    if item_id not in items:
        HTTPException(status_code=400, detail=f"Item with {item_id=} does not exists.")
    if all(info is None for info in (name, price, count)):
        raise HTTPException(status_code=400, detail="No parameters provided for the update")

    item = items[item_id]
    if name is not None:
        item.name = name
    if price is not None:
        item.price = price
    if count is not None:
        item.count = count

    return {"updated": item}


@api_app.delete("/delete/{item_id}")
def delete_item(item_id: int) -> dict[str, Item]:

    if item_id not in items:
        raise HTTPException(status_code=400, detail=f"Item with {item_id=} does not exists.")

    item = items.pop(item_id)
    return {"deleted": item}
