import requests

# request error, price and count are negative number :
print(requests.put("http://127.0.0.1:8000/update/0?count=-1").json())
print(requests.put("http://127.0.0.1:8000/update/0?price=-1").json())

# request error, item_id cannot be negative :
print(requests.put("http://127.0.0.1:8000/update/-1").json())

# request error, name exceeds 8 chars :
print(requests.put("http://127.0.0.1:8000/update/0?name=CyberMouseExtra").json())
