import requests

# working queries requests :
print(requests.get("http://127.0.0.1:8000/items?count=10").json())
print(requests.get("http://127.0.0.1:8000/items?category=software").json())

# request failed due to wrong category, not in Category enum :
print(requests.get("http://127.0.0.1:8000/items?category=games").json())

# request fail as count not integer :
print(requests.get("http://127.0.0.1:8000/items?count=10.0").json())
print(requests.get("http://127.0.0.1:8000/items?count=ten").json())


# Pydantic error :
print(
    requests.post(
        "http://127.0.0.1:8000/",
        json={"name": "Keyboard", "price": 19.99, "count": 10, "id": 4},
    ).json()
)
