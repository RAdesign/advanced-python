"""simple fastAPI application, using pydantic for validation, and uvicorn for server emulation,
initialize uvicorn with proper parameters (comments at the end), and then run requests, here,
with x_no_main.py files"""
from enum import Enum

# import uvicorn  # for no 1. method of running this, comment at the end
from pydantic import BaseModel
from fastapi import FastAPI, HTTPException

# fast api instance
api_app = FastAPI()


# helper classes
class Category(Enum):
    HARDWARE = 'hardware'
    SOFTWARE = 'software'


# fastAPI relies on pydantic for all validation
class Item(BaseModel):
    name: str
    price: float
    count: int
    id: int
    category: Category


# a dictionary with items, containing pydantic objects
# instead of a database access ('alchemy' would be used instead for database)
items = {
    0: Item(name="FloppyDr", price=99.99, count=5, id=0, category=Category.HARDWARE),
    1: Item(name="Mouse", price=9.99, count=20, id=1, category=Category.HARDWARE),
    2: Item(name="Win3.11", price=69.99, count=10, id=2, category=Category.SOFTWARE)
}


# most basic query to get all items
@api_app.get("/")
def index() -> dict[str, dict[int, Item]]:
    return {"items": items}


# Path parameters can be specified with {} directly in the path (as in f-strings)
# Those parameters will be forwarded to the decorated function as keyword arguments
@api_app.get("/items/{item_id}")
def query_item_by_id(item_id: int) -> Item:
    if item_id not in items:
        HTTPException(status_code=404, detail=f"Item with Id: {item_id=} does not exists.")

    return items[item_id]


# function parameters are not path parameters, and can be specified as query parameters in URL
# get query items like this : /items?count=20
Selection = dict[str, str | int | float | Category | None]  # this dictionary has users query arguments


@api_app.get("/items")
def query_item_by_parameters(
        name: str | None = None,
        price: float | None = None,
        count: int | None = None,
        category: Category | None = None,
) -> dict[str, Selection | list[Item]]:
    def check_item(item: Item):
        # Check if the item matches the query arguments from outer scope
        return all(
            (
                name is None or item.name == name,
                price is None or item.price == price,
                count is None or item.count != count,
                category is None or item.category is category,
            )
        )
    selection = [item for item in items.values() if check_item(item)]
    return {
        "query": {"name": name, "price": price, "count": count, "category": category},
        "selection": selection,
    }


# # 1 method how to run uvicorn, initialize it from within file :
# if __name__ == "__2_create_get_route_query__":
#    uvicorn.run(api_app, host="127.0.0.1", port=8000)
# # run in terminal : python FastAPIPython.2_create_get_route_query.py

# 2 method how to run uvicorn, calling uvicorn command :
# terminal : uvicorn FastAPIPython.2_create_get_route_query:api_app --reload
# a loading error will show, when not properly setting a directory of script vs directory of uvicorn
# remember to use "." instead of "/", as it sees is as a module
# necessary run/debug configuration: script path -> module name : uvicorn ;
# Parameters: 2_create_get_route_query:api_app --reload , optional: (--port 8080)
# this debug configuration allows for reloading and debug at same time
