from enum import Enum
from fastapi import FastAPI
from pydantic import BaseModel

api_app = FastAPI()


class Category(Enum):
    HARDWARE = 'hardware'
    SOFTWARE = 'software'


class Item(BaseModel):
    name: str
    price: float
    count: int
    id: int
    category: Category


items = {
    0: Item(name="FloppyDr", price=99.99, count=5, id=0, category=Category.HARDWARE),
    1: Item(name="Mouse", price=9.99, count=20, id=1, category=Category.HARDWARE),
    2: Item(name="Win3.11", price=69.99, count=10, id=2, category=Category.SOFTWARE)
}


# FastAPI handles serialization and deserialization of JSON.
# Possible use of built-in Python and Pydantic types , here : dict[int, Item]
@api_app.get("/")
def index() -> dict[str, dict[int, Item]]:
    return {"items": items}
