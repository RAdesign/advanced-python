from fastapi import FastAPI, Path
from typing import Optional

test_app = FastAPI()


inventory = {
    1: {
        "name": "Mouse",
        "price": 1900,
        "brand": "Yogitech"
    }
}


"""@test_app.get("/get-item/{item_id}")
def get_item(item_id: int):   # automatically verifies type of value
    return inventory[item_id]"""


"""@test_app.get("/get-item/{item_id}/{name}")  # multiple path parameters inside endpoint
def get_item(item_id: int, name: str):
    return inventory[item_id]"""


# using Path reference
@test_app.get("/get-item/{item_id}}")
def get_item(item_id: int = Path(None, description="Unique Item ID you want to view", gt=0, lt=3)):
    return inventory[item_id]


# now with query parameters
@test_app.get("/get-by-name")
def get_item(*, name: str = None, test: int):
    # str = None causes query parameter to be non-obligatory, otherwise an error w/o a query
    # query: host/get-by-name?test=3&name=Mouse
    # can also use : (name: Optional[str] = None)
    # for python sequence of args: (test: int, name: Optional[str] = None) or (*, name: Optional[str] = None, test: int)
    for item_id in inventory:
        if inventory[item_id]["name"] == name:
            return inventory[item_id]
    return {"Data": "Not found"}
# usage : host/get-by-name?name=Mouse
