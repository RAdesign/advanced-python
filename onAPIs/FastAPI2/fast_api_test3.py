from fastapi import FastAPI, Path, Query
from typing import Optional
from pydantic import BaseModel

test_app = FastAPI()


class Item(BaseModel):
    name: str
    price: float
    brand: Optional[str] = None


"""inventory = {
    1: {
        "name": "Mouse",
        "price": 1900,
        "brand": "Yogitech"
    }
} # used for inserting JSON, not objects"""
inventory = {}


@test_app.get("/get-item/{item_id}}")
def get_item(item_id: int = Path(None, description="Unique Item ID you want to view", gt=0, lt=9)):
    return inventory[item_id]


# now with query parameters and Path parameters combined
@test_app.get("/get-by-name/{item_id}")
def get_item(name: str = Query(None, title="Name", description="a name you want")):
    for item_id in inventory:
        # if inventory[item_id]["name"] == name:  # just for inserting as a dictionary
        if inventory[item_id].name == name:
            return inventory[item_id]
    return {"Data": "Not found"}
# combined query: HOST/get-by-name/1?test=3&name=Mouse


# request body query, using pydantic BaseModel for object, basic:
"""@test_app.post("/create_item")  # does not need to specify request params, as it presumes it will be in request-body
def create_item(item: Item):
    return {}"""


# for item insertion :
@test_app.post("/create_item/{item_id}")
def create_item(item_id: int, item: Item):
    if item_id in inventory:
        return {"Error:": "Item already in inventory."}

    # inventory[item_id] = {"name": item.name, "brand": item.brand, "price": item.price}  # ineffective way to insert
    inventory[item_id] = item
    return inventory[item_id]

