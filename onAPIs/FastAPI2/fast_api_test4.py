from fastapi import FastAPI, Path, Query, HTTPException, status
from typing import Optional
from pydantic import BaseModel

test_app = FastAPI()


class Item(BaseModel):
    name: str
    price: float
    brand: Optional[str] = None


class UpdateItem(BaseModel):  # class for updating w/optionals
    name: Optional[str] = None
    price: Optional[float] = None
    brand: Optional[str] = None


inventory = {}


@test_app.get("/get-item/{item_id}}")
def get_item(item_id: int = Path(None, description="Unique Item ID you want to view", gt=0, lt=9)):
    return inventory[item_id]


# now with query parameters and Path parameters combined
@test_app.get("/get-by-name/{item_id}")
def get_item(name: str = Query(None, title="Name", description="a name you want")):
    for item_id in inventory:
        if inventory[item_id].name == name:
            return inventory[item_id]
    # using HTTPException, status , for customary responses :
    # raise HTTPException(status_code=404, detail="This item not found")
    raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)


# for item insertion :
@test_app.post("/create_item/{item_id}")
def create_item(item_id: int, item: Item):
    if item_id in inventory:
        # return {"Error:": "Item already in inventory."}
        raise HTTPException(status_code=400, detail="This item already exists")  # customary message

    inventory[item_id] = item
    return inventory[item_id]


# doing PUT method
@test_app.put("/update-item/{item_id}")
def update_item(item_id: int, item: Item):
    if item_id not in inventory:
        return {"Error:": "Item does not exists."}
    # this is because item is an instance of an object, not a python dictionary anymore
    if item.name is not None:
        inventory[item_id].name = item.name
    if item.price is not None:
        inventory[item_id].price = item.price
    if item.brand is not None:
        inventory[item_id].brand = item.brand
    return inventory[item_id]


# doing DELETE method
@test_app.delete("/delete-item")
def delete_item(item_id: int = Query(..., description="The ID of item to be deleted", gt=0)):
    if item_id not in inventory:
        return {"Error:": "Item does not exists,"}
    del inventory[item_id]
    return {"Success:": "Item removed"}
