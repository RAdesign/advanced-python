from fastapi import FastAPI

test_app = FastAPI()


@test_app.get("/")
def basic():
    return {"Data": "confirmed"}  # any data returned form endpoint is turned to JSON
# in web browser use host/docs to get FastApi documentation


@test_app.get("/about")
def about():
    return {"Data": "about"}
