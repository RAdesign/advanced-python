"""log papertrail - a logging service, one of many, and how to integrate it, don`s send important
 information to logs, as they can be compromised"""
import logging
from logging.handlers import SysLogHandler

PAPERTRAIL_HOST = "logs.papertrailapp.com"  # host of logging service
PAPERTRAIL_PORT = 44000  # just put your assigned port


def main() -> None:  # different parameters of a logging service
    logger = logging.getLogger("myLogger")
    logger.setLevel(logging.DEBUG)
    handler = SysLogHandler(address=(PAPERTRAIL_HOST, PAPERTRAIL_PORT))  # external log service handler
    logger.addHandler(handler)

    logger.debug("Debug logger tes message")


if __name__ == "__main__":
    main()
