"""how to log to a file, a file is created, and is put there, new logs will be added to existing file
in none exists one will be created and updated. Security - keep log storage safe, delete old logs."""
import logging


def main() -> None:
    logging.basicConfig(
        level=logging.DEBUG,
        format="%(asctime)s %(levelname)s %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
        filename="application.log",
    )

    logging.debug("A debug message")
    logging.info("An info message")
    logging.warning("A warning message")
    logging.error("An error message")
    logging.critical("A critical message")


if __name__ == "__main__":
    main()
