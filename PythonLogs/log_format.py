"""standard formatting of a log, apart from levels gives formatting data"""
import logging


def main() -> None:
    logging.basicConfig(   # different parameters of how a log should look
        level=logging.DEBUG,
        format="%(asctime)s %(levelname)s %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
    )

    logging.debug("A debug message")
    logging.info("An info message")
    logging.warning("A warning message")
    logging.error("An error message")
    logging.critical("A critical message")


if __name__ == "__main__":
    main()
