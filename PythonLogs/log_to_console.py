"""how to log to console, with level showing different logs to show
Important! - logs can lead to data leaks"""
import logging


def main() -> None:
    logging.basicConfig(level=logging.WARNING)  # this level shows warning, error and critical
    # different levels of logging depth
    logging.debug("A debug message")
    logging.info("An info message")
    logging.warning("A warning message")
    logging.error("An error message")
    logging.critical("A critical message")


if __name__ == "__main__":
    main()
