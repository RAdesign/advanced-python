"""A python file for quick and smart one-liners, and tricks """

# 1. How to interchange variables quick

a = 11
b = 7
a, b = b, a

# 2. How to comprehend a list in one line, even with flow control
sqr = []
for item in range(10):
    if item % 2 == 0:
        sqr.append(item*item)
# one liner :
sqr = [item*item for item in range(10) if item % 2 == 0]

# 3. if else ternary operator in one line
var = 23 if 7 > 9 else 32

# 4. print elements in collection in one line, but just contents
array = [1, 2, 3, 5, 7, 11]

for item in array:
    print(item, end=" ")  # unfortunately with a space at the end

# how to in one liner :
print(*array)  # just elements without space at end

# 5. counting days left in a year
import datetime;print((datetime.date(2023,12,31)-datetime.date.today()))  # can be applied in terminal

# 6. reverse a list (or a string which is a list)
array = array[::-1]
print(array == array)  # palindrome check

# 7. assign multiple variables
name, language, cash = "John", "eng", True
print(name, language, cash)

# 8. number string to integer list
my_input = "1 2 3 5 7 11"
my_list = my_input.split()
print(my_list)  # elements remain a string

my_list_2 = list(map(int, my_input.split()))  # now it`s in integers
print(my_list_2)

# 9. read file into list
# new_list = [line.strip() for line in open("my_file.txt", "r")]  # prints lines as items w/o whitespaces

# 10. local http server
# python -m http.server  # run in terminal to create a localhost in a directory

# 11. Iterate with enumerate instead of range(len(x))
array2 = [1, 3, -5, -11]
for item in range(len(array2)):
    if array2[item] < 0:
        array2[item] = 0
print(array2)
# using enum returns a tuple of index and value, allows access to item with index:
array2 = [1, 3, -5, -11]
for idx, value in enumerate(array2):
    if value < 0:
        array2[idx] = 0
print(array2)
# or, simple iteration:
for idx, value in enumerate(array2):
    print(idx, value)

# 12. sort complex iterables with built-in sorted(). Can use any, like tuple, but result will be a list anyway
array3 = [1, 3, 11, -1, 8, 19, 23]
sorted_array3 = sorted(array3)
sorted_array4 = sorted(array3, reverse=True)
print(sorted_array3, "\n", sorted_array4)
# now for the complex array:
complex_array = [{"name": "John", "age": 6},
                 {"name": "Janine", "age": 66},
                 {"name": "John", "age": 16}, ]
sorted_complex_1 = sorted(complex_array, key=lambda x: x["age"])
print(sorted_complex_1)

# 13. store unique values with Sets - convert to use Sets properties
array_repeated_val = [1, 1, 2, 3, 5, 7, 7, 7, 11]
converted_set = set(array_repeated_val)
print(converted_set)

set_of_primes = {2, 3, 5, 7, 11, 13, 17, 19, 23}  # or just make a Set at start, if list will have uniques anyway

# 14. save memory with Generators - not every time a list is the best solution
import sys  # for measuring object sizes
large_list = [item for item in range(10000)]
print(sum(large_list))
print(sys.getsizeof(large_list), "bytes")

# use generator comprehension instead. Generator - () instead of [] - works slower, but only when called, 1 item at time
large_generator = (item for item in range(10000))
print(large_generator)
print(sys.getsizeof(large_generator), "bytes")
# use generator for large data pools

# 15. Define default values in Dictionaries with .get() and .setdefault()
my_dict = {"item": "basketball", "price": 50}
print(my_dict)
# count = my_dict["count"] # this ends with KeyError
count = my_dict.get("count")  # this returns default value - None
print(count)
print(my_dict)
count = my_dict.get("count", 0)  # this returns default value we provided
print(count)
print(my_dict)
count = my_dict.setdefault("count", 0)  # this sets this default
print(count)
print(my_dict)  # change in key:value visible

# 16. Count hashable objects with collections.Counter
from collections import Counter
count_list = [1, 1, 2, 3, 5, 7, 7, 7, 11, 13, 17, 19]
my_counter = Counter(count_list)
print(my_counter)  # returns sequence of pairs, item:item_frequency, sorted from most common to least
print(my_counter[7])  # to get how many specific items there are, 0 for not present
most_common_item = my_counter.most_common(2)  # returns x most common items, as list of tuples (item, count)
print(most_common_item)
print(most_common_item[0][0])  # to get just most common number

# 17. format strings with f-strings
time = "afternoon"
my_time = f"It`s {time}"
print(my_time)
my_val = 10
print(f"Values are calculated at runtime, {my_val} squared is {my_val*my_val}")

# 18. Concatenate strings with "".join
strings_array = ["how", "to", "join", "this"]
ready_string = " ".join(strings_array)
print(ready_string)

# 18. Merging dictionaries with {**d1, **d2}   python(3.5+)
dict_1 = {"name": "Janine", "age": 66}
dict_2 = {"name": "Janine", "city": "Malbork"}
merged_dict = {**dict_1, **dict_2}
print(merged_dict)

# 19. Simplify if-statements with if x in [a,b,c]
colours = ["magenta", "yellow", "cyan", "key-colour"]
c = "magenta"
if c == "magenta" or c == "yellow" or c == "cyan":
    print("is main colour")
# simple :
if c in colours:
    print("is main colour")

# 20. ternary operator - on typical if/else one liner, do not use if it gets unreadable
condition = False
x = 1 if condition else 0

# 21. calculating large numbers, underscore do not affect the program
large_number = 10_000_000_000
print(f"{large_number:,}")

# 22. manually managing open-close file : use context manager
"""with open('text.txt', 'r') as file:
    file_content = file.read()
words = file_content.split(' ')
word_count = len(words)
print(word_count)"""
# can also be used to manage threads, database connections, managing other resources

# 23. enumerate is good, but zip is better:
# pair values while iterating with enum
letters_cap = ["A", "B", "C", "D", "E"]
letters_small = ["a", "b", "c", "d", "e"]
letters_type = ["vowel", "consonant", "consonant", "consonant", "vowel"]
for index, letter_cap in enumerate(letters_cap):
    letter_sm = letters_small[index]
    print(f"{letter_cap} in low cap is {letter_sm}")
# the same wit python zip function - much cleaner, can unpack extra values
for index_cap, letter_cap, letters_type in zip(letters_cap, letters_small, letters_type):
    print(f"{index_cap} in low cap is {letter_cap}, and it is a {letters_type}")
for value in zip(letters_cap, letters_small, letters_type):  # print tuples of all of those values
    print(value)

# 24. unpacking values
c, b = (1, 2)  # to avoid interpreter prompts, or as convention, replace unused, but unpacked variable with underscore
a, _ = (1, 2)
print(a)
# print(b)
# z, y, x = (1, 2, 4, 5, 7)   # too many values to unpack error
z, y, *x = (1, 2, 4, 5, 7)  # x unpacks all leftover values now
z1, y1, *x1, v1 = (1, 2, 4, 5, 7, 11, 13, 17)  # all unmatched values go to x1, v1 to last, it adjusts itself
print(z1, y1)
print(x1)
print(v1)
# z1, y1, *_, v1 = (1, 2, 4, 5, 7, 11, 13, 17)   # for ignoring multiple values


# 25. getting or setting attributes on a certain object

class Animal:
    pass
animal = Animal()
# dynamic class modification, attributes
animal.first = "cat"
animal.second = "dog"
print(animal.first)

# what if new attribute to set is a value of a specific key, use setattr()
first_key = "first"
first_value = "dog"

setattr(animal, "first", "dog")  # changes value of first attribute
print(animal.first)  # attr changed
setattr(animal, first_key, first_value)  # works as well with variables
# now use with getattr(), usable when want to iterate through attributes of objects
first = getattr(animal, first_key)
print(first)

# iterating with setattr ,getattr
animal_info = {"first": "Cat", "second": "Dog"}
for key, value in animal_info.items():
    setattr(animal, key, value)

for key in animal_info.keys():
    print(getattr(animal, key))

# 26. inputting secret information, using getpass inbuilt function
from getpass import getpass
username = input("Username: ")
password = getpass("Password:" )    # makes input covered
print("logging in")

# 27. running python with -m prompt
# example: python -m venv my_env   (run python virtual environment)
# example: python -m smtpd -c DebuggingServer -n localhost:34345 (debug email server in python
# -m - search sys.path for named module and run it, with arguments
# use help function on modules to learn how to use modules
# use dir(module) to print out all available attributes, method


# 28. Use f-strings instead of concat
# 29. Use 'with' statement for opening-closing files/threads
# 30. Use context manager instead of try - finally.
# 31. Use specific exceptions

# 32. For using mutable defines inside function declaration, define None and then check.
def append(n, mut=None):
    if mut is None:
        mut = []
    mut.append(n)
    return mut

# 33. Rethink using comprehensions too often, if only - list comprehensions
# 34. Avoid checking equality with ==, (because of inheritance issues - Liskov substitution violation)
# maybe use 'isinstance'

# 35. Avoid using == for checking True/False, prefer checking for identity using 'is'
# 36. Use straight 'if x' instead of 'if bool' or 'if len'

# 37. Avoid using range(len(array)) for looping for index, loop 'for element in array:' directly
# if looping index is necessary, try using enumerate(array) to get index and element at the same time

# 38. For iterating within nested arrays/lists use 'zip(a1, a2)' or if index needed enumerate(zip(a1, a2))
# 39. Key is the default for looping through a dictionary
# 40. Use dictionary items method - for key, value in...
# 41. Use tuple unpacking for extracting each value to variable
# 42. Use 'for i, x in enumerate(list):' instead of creating incrementing index variable
# 43. Use time.perf_counter() instead of time.time() for measuring time. Just subtract 2 vars of t.perf_count
# 44. Try using logging module instead of print() statements.
# 45. For subprocesses avoid Shell=True as it causes many problems, just put arguments into a list
# 46. Try using NumPy and Pandas for doing maths and math analysis in Python
# 47. Outside interactive session avoid using import *
# 48. Reduce dependency on one directory structure, use Python packages for using own modules

# 49. Python is compiled to bytecode not machine-code
# (.pyc files , __pycache__ directory contains compiled files) and interpreted

# 50. Try using PEP-8 style guide, some outtakes:
# - 80 characters max for a line
# - use auto-formatter, like "black"
# - spaces for indentations preferred to TABs
# - be consistent with style
# - variables and functions with snake_case (no PascalCase or camelCase)
# - CONST with all uppercase, try using at top of statements
# - modules in lower case, snake_case
# - classes, exceptions in PascalCase
# - self ~ cls
# - import hierarchy , on top, sequence of importance, separate line for module
# - use 'x is None' instead of '==', use 'if x is not X'
# - always make Exception a specific case
# - use string.startswith("something") or string.endswith("smth"), instead of if string[:3] == "smth"


# 51. Try using default dictionaries (dict) if possible. Example of counting repetition of each number:
"""from collections import defaultdict
count_dict = defaultdict(lambda: 0)  # creates a default dict with first value 0
numbers_dict = [1, 2, 4, 3, 2, 3, 4, 1, 6, 5]
for key in numbers_dict:
    count_dict[key] += 1
print(count_dict)"""
# Another fast way to do similar thing without defaultdict:
"""dict_51 = {}
if 'default' not in dict_51:
    dict_51['default'] = []
dict_51['default'].append(23)
# in one line, setdefault sets value for accessed key, if the value does not exist, otherwise - returns its value:
dict_51.setdefault('default', []).append(23)   """

# 52. Do not override built-in keywords, never ever.




