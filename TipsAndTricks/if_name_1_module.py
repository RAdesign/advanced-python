"""explanation of using if-nam structure and it`s implications - first module
Basic reason is to make sure, that code is run directly from main, but not run when imported"""

print("This will be run regardless")  # a check that import runs uncalled


def main():
    pass


if __name__ == "__main__":
    main()

# before running Python sets some variables, like name to main
print(__name__)  # returns __main__
print("First Module`s name is : {}".format(__name__))  # returns __main__
# when importing modules, __name__ variable is set to the name of the file

# see module_2
# Basic sense of this structure is to check, if this current file is being run directly by Python, or it`s imported
# check running this first in module_1, then module_2:
if __name__ == "__main__":
    print("Run directly")
else:
    print("Run from import")
