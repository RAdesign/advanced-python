"""explanation of using if-nam structure and it`s implications - second module.
Imported module is being run"""
import if_name_1_module

# after import interpreter returns : First Module`s name is: if_name_1_module
# Because the file is not being run directly, but imported

# now, printing causes to run statement, and it`s name is __name__ again, because Python is running it directly
print("Second Module`s name is : {}".format(__name__))

# this structure allows for choice of running imports, not auto run
# all within main method will be run just after calling, but all outside - will be run on import:
if_name_1_module.main()
