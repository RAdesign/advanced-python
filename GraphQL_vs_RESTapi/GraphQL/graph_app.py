from ariadne import graphql_sync
from ariadne.constants import PLAYGROUND_HTML  # from github of graphql
from flask import Flask, jsonify, request

from scheme.create import create_schema

graph_app = Flask(__name__)  # still flask for a server
# ariadne library (one of many) for defining graphQL types server


# for creating automated web queries server
@graph_app.route("/graphql", methods=["GET"])
def graphql_playground():
    return PLAYGROUND_HTML, 200


# main graphQL interface
@graph_app.route("/graphql", methods=["POST"])
def graphql_server():
    # GraphQL query is always sent as POST method and json
    query_data = request.get_json()

    # passing the request to the context is optional
    # in Flask current request is always accessible as flask.request
    this_schema = create_schema()
    success, result = graphql_sync(this_schema, query_data, context_value=request, debug=graph_app.debug)

    status_code = 200 if success else 400
    return jsonify(result), status_code


if __name__ == "__main__":
    graph_app.run(debug=True)
