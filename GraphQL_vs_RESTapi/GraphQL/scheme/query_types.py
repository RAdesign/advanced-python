from ariadne import MutationType, QueryType

# all that will be exposed to queries
MAIN_TYPE_DEF = """
    type Query {
        blogs: [Blog]!
        blog(id: ID!): Blog!
        authors: [Author]!
        author(id: ID!): Author!
    }
"""

# necessary definitions of graphQL actions, a query and mutation
query = QueryType()
mutation = MutationType()
