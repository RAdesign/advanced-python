from typing import Any

from ariadne import ObjectType
from ..graph_data import Blog, BlogPayload, all_blogs, get_blog, get_author, update_blog
from graphql import GraphQLResolveInfo

from query_types import mutation, query

# all that will be exposed for queries, data structure
BLOG_TYPE_DEF = """
    type Blog {
        id: ID!
        title: String!
        content: String!
        author: Author!
    }
    
    input BlogPayload {
        title: String
        content: String
    }

    type Mutation {
        update_blog(id: ID!, payload: BlogPayload!): Blog
    }
"""

blog_query = ObjectType("Blog")  # ariadne query object, that can be added elswhere


@query.field("blogs")
def resolve_blogs(_, info: GraphQLResolveInfo) -> list[Blog]:
    return all_blogs()


@query.field("blog")
def resolve_blog(_, info: GraphQLResolveInfo, id: str) -> Blog:
    return get_blog(int(id))


@mutation.field("update_blog")
def resolve_bog_update(_, info: GraphQLResolveInfo, id: str, payload: BlogPayload) -> Blog:
    return update_blog((int(id)), payload)


@blog_query.field("author")
def resolve_blog_author(blog: dict[str, Any], info: GraphQLResolveInfo):
    print(blog)
    return get_author(blog["author_id"])
