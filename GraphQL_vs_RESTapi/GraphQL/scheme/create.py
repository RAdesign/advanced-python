from ariadne import make_executable_schema
from graphql.type.schema import GraphQLSchema

from author import AUTHOR_TYPE_DEF
from blog import BLOG_TYPE_DEF, blog_query
from query_types import MAIN_TYPE_DEF, mutation, query


# basic graphQL schema , query - getting data, mutation - changing data
def create_schema() -> GraphQLSchema:
    return make_executable_schema(
        [MAIN_TYPE_DEF, BLOG_TYPE_DEF, AUTHOR_TYPE_DEF], [query, blog_query, mutation]
    )
