Comparisons between using RESTapi and GraphQL
1. REST Representation State Transfer is quite old.
- resource oriented (new state of resource passed)
- be assured your REST is up to standard, take Swagger
- needs to coordinate several requests on the frontend (slowing down ux)
- no distinction between structure of database and structure of data moved via API (sending DB to DB -> security issues)
- bad to control how much data requests get
2. gRPC
- action oriented (calling remote function to perform certain task and get a result feedback)
- single endpoint and query language (objects like graphs connections)
- can define API on backend by creating a schema - solving security issues
- did not have to specify the endpoints, just modify the queries

3. Graph vs REST:
- con of graph - sending request to the server is more complicated, have to specify query/mutation 
in REST it`s just encoded in endpoints
- graph suffers from m+1 problem of databases, for each query(ex Author) a new db will be created, a local
caching mechanism needs to be implemented to mitigate this, or a mechanism to group resolves in graphQL,
and libraries for that are lacking, 
- GraphQL-language can be difficult to use 
- not everything is standardized, ex. search with pagination, courser, etc.
- REST is simple - good for small applications and public APIs, 
but security is harder to maintain (layer architecture may solve this)
- GraphQL for larger applications with specified data, frontend is easier to manage