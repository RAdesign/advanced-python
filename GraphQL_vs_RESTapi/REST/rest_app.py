"""this runs both test server and response is in browser in localhost and port parameters"""
from flask import Flask, jsonify, request

from rest_data import all_blogs, all_authors, get_blog, get_author, update_blog

blog_app = Flask(__name__)


@blog_app.route("/")  # returns text in browser window with localhost
def route_hello():
    return "Hello, readers !"


@blog_app.route("/blogs")  # returns all blogs, add "/blogs" to localhost address in browser
def route_all_blogs():
    return jsonify(all_blogs())


@blog_app.route("/blogs/<blog_id>", methods=["GET"])  # return a blog with a specific id (<id>in route, as in Flask)
def route_get_blog(blog_id: str):
    return jsonify(get_blog(int(blog_id)))


# update blog: curl -X POST -H "Content-Type: application/json" \
# -d '{"title": "This blog was changed" }' \
# http://127.0.0.1:5000/blogs/3     (or other host)

@blog_app.route("/blogs/<blog_id>", methods=["POST"])  # or PUT
def route_update_blog(blog_id: str):
    payload = request.get_json()
    return jsonify(update_blog(int(blog_id), payload))


@blog_app.route("/authors")
def route_all_authors():
    return jsonify(all_authors())


@blog_app.route("/authors/<name_id>")
def route_get_author(name_id: str):
    return jsonify(get_author(int(name_id)))


if __name__ == "__main__":
    blog_app.run(debug=True)
