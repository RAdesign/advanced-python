"""provides a number of basic operations to do with data of the rest_app"""
from typing import TypedDict


# a blog class
class Blog(TypedDict):
    blog_id: int
    title: str
    content: str
    author_id: int


# used when uploading to a blog, updating
class BlogPayload(TypedDict, total=False):
    title: str
    content: str


class Author(TypedDict):
    name_id: int
    name: str


# ready to test pre-prepared data, instead a database
BLOGS: dict[int, Blog] = {
    1: {"blog_id": 1, "title": "Blog no 1", "author_id": 1, "content": "blog 1 content"},
    2: {"blog_id": 2, "title": "Blog no 2", "author_id": 2, "content": "blog 2 content"},
    3: {"blog_id": 3, "title": "Blog no 3", "author_id": 3, "content": "blog 3 content"},
}

AUTHORS: dict[int, Author] = {
    1: {"name_id": 1, "name": "Author Named 1"},
    2: {"name_id": 2, "name": "Author Named 2"},
    3: {"name_id": 3, "name": "Author Named 3"},
}


class NotFoundError(Exception):
    pass


def all_blogs() -> list[Blog]:
    return list(BLOGS.values())


def get_blog(blog_id: int) -> Blog:
    if not BLOGS.get(blog_id):
        raise NotFoundError("Blog not found")
    return BLOGS[blog_id]


def update_blog(blog_id: int, payload: BlogPayload) -> Blog:
    blog = BLOGS.get(blog_id)
    if not blog:
        raise NotFoundError("Blog not found")
    for key, value in payload.items():
        blog[key] = value
    return blog


def all_authors() -> list[Author]:
    return list(AUTHORS.values())


def get_author(author_id: int) -> Author:
    if not AUTHORS.get(author_id):
        raise NotFoundError("Author not found")
    return AUTHORS[author_id]
