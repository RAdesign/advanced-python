Some more advanced exercises in Python.
1.Practice on how to build Fast API with Python.
2.REST API vs. GraphQL examples
3.Logs with Logging library in Python
4.Pydantic vs. Dataclasses vs. ATTRs
5.Virtual Environments, Poetry example
6.Unit Testing with Unittest Module
7.Some uses of Supabase
8.Comprehension on functions in Python
Last.General Tips and Tricks
